class AccountActivationsController < ApplicationController
  include AccountActivationsHelper

  def activate
    user_email = params[:email]
    user_activation_token = params[:id]

    # chamar endṕoint pra ativar usuário
    reply = request_activation(user_email, user_activation_token)
    # salvar o token de acesso para já usar a plataforma, se tudo der certo
    save_session reply.params['auth_token'] if reply.success

    # responde
    render status: reply.status, json: { message: reply.message }   
  end

end


class API::V1::ActionsController < ApiController

  before_action :load_saved_game, :init_hash

  def index
    actions = Action.where(game_id: @game.id, hidden: false, removed: false)

    @hash[:data] = actions.present?? actions.collect { |a| a.to_hash(:api)} : []

    respond_with @hash
  end

  def show
    action = Action.find_by_game_id_and_verb(@game.id, params[:verb])

    @hash[:data] = action.present?? action.to_hash(:api) : []

    respond_with @hash
  end

private

  def init_hash
    @hash = {}
  end

  def load_saved_game
    @game = Game.find_by_unique_name(params[:unique_name])
  end
end
class CreatePlayerBadges < ActiveRecord::Migration
  def change
    create_table :player_badges do |t|
      t.references :player, index: true, foreign_key: true
      t.references :badge, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class ServiceResponse
	attr_accessor :success, :status, :message, :params

	def initialize success, options={}
		@success = success
		@status = options[:status]
		@message = options[:message]
		@params = options[:params]
	end
end
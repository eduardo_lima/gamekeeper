class CreateActionObjects < ActiveRecord::Migration
  def change
    create_table :action_objects do |t|
      t.string :article
      t.string :singular
      t.string :plural

      t.references :action, index: true, foreign_key: true
    end
  end
end

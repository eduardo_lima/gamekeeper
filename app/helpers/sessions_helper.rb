module SessionsHelper

#-----------------------------------------------------------------
# Helpers internos
# São métodos para salvar sessão independentes de aplicações 
# externas
#
  def save_session token
    session[:auth_token] = token
  end

  def delete_session
    session.delete(:auth_token)
  end

  def remember_session token
    cookies.permanent[:auth_token] = token
  end

  def forget_session 
    cookies.delete(:auth_token)
  end

  def get_current_session
    if (auth_token = session[:auth_token])
      auth_token
    elsif (auth_token = cookies[:auth_token])
      auth_token
    end
  end

  def logged_in?
    @current_user.present?
  end

#-----------------------------------------------------------------
# Helpers externos
# São métodos para obter informações do usuário e sessão do
# gatekeeper
#
  def send_signin_request email, password    
    GatekeeperService.new.signin(email, password)
  end

  def send_current_user_request 
    GatekeeperService.new.current_user(get_current_session)
  end

  def request_sign_out
    reply = GatekeeperService.new.signout get_current_session

    # esquecer sessão do usuário
    if reply.success 
      delete_session
      forget_session
    end
   
    reply
  end
end

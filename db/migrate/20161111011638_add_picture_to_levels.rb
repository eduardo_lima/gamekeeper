class AddPictureToLevels < ActiveRecord::Migration
  def change
    add_column :levels, :picture, :string
  end
end

class UserMailer < ApplicationMailer
  include Rails.application.routes.url_helpers

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation(user)
    @user = user
    # colocar request em um arquivo de ambiente depois
    @base_url = "http://localhost:3000/"
    mail to: user.email, subject: "Account activation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    @base_url = "http://localhost:3000/"
    mail to: user.email, subject: "Password reset"
  end

end

(function () {
  
  var GameBadgeCtrl = ['$scope', 'fileReader', 'labelsFactory', 'gameBadgeFactory', 'gameActionFactory', 'toastr', 'serialGenerator',
    function ($scope, fileReader, labelsFactory, gameBadgeFactory, gameActionFactory, toastr, serialGenerator) {

    // carrega as labels dos componentes da html
    $scope.labels = labelsFactory.gameBadges();
    // carrega as badges cadastradas no game
    $scope.gameBadges = []; // lembrar do serviço

    $scope.badgesAreLoaded = false;
    
    gameBadgeFactory.start();
    gameActionFactory.start();
    
    // ############ LISTAR
    gameBadgeFactory.list($scope.game.id).then(function (results) {
      $scope.gameBadges = results.data;

      $scope.badgesAreLoaded = true;
    });

    gameActionFactory.list($scope.game.id).then(function (results) {
      $scope.availableActions = results.data;
    });


    // ############ ADICIONAR
    $scope.new = function () {

      // aqui será configurado de como a badge será representada no form
      $scope.gameBadge = {
        id: undefined,
        isSelected: true,
        game_id: $scope.game.id,
        // inicializar a imagem
        image_url: '/assets/game-badge-icon.png',
        name: '',
        bonus_points: '',
        active: true,
        badge_reward_condition: {
          condition_type: '',
        },
        badge_reward_actions: []
      };

      // iterar e desselecionar tudo
      $scope.gameBadges.find(function (e, i, a) {
        e.isSelected = false;
      })

      $scope.gameBadges.push($scope.gameBadge);
    };
      
    // ############ SELECIONAR
    $scope.select = function (gameBadge) {
      // iterar e desselecionar tudo
      $scope.gameBadges.find(function (e, i, a) {
        e.isSelected = false;
      })

      // selecionar agora =)
      $scope.gameBadge = gameBadge;
      $scope.gameBadge.isSelected = true;

      // gerar serial pra melhor controlar o array
      // $scope.gameBadge.conditions.performed_actions.find(function (e, i, a) {
      //   e.serial = serialGenerator.generate();
      // });
    };

    // ############ ANEXAR IMAGEM
    $scope.getFile = function () {
        fileReader.readAsDataUrl($scope.gameBadge.file, $scope).then(function(result) {
          $scope.gameBadge.image_url = result;
        });
      };

    // ############ ADICIONAR ACTIONS NA CONDICAO
    $scope.addPerformedAction = function () {
      $scope.gameBadge.badge_reward_actions.push({
        id: undefined,
        times_to_achieve: undefined,
        serial: serialGenerator.generate()
      })
    };
    // ############ REMOVER ACITONS NA CONDICAO
    $scope.removePerformedAction = function (action) {
      var indexToRemove = undefined;
      $scope.gameBadge.badge_reward_actions.find(function (e, i, a) {
        if (e.serial == action.serial) {
          indexToRemove = i;
        }
      });
      $scope.gameBadge.badge_reward_actions.splice(indexToRemove, 1);
    };
  
    // ############ ARMAZENAR BADGE
    $scope.save = function (gameBadge) {

      $scope.isBadgeProcessing = true;

      // ver se é edição ou criação, colocar condição aqui
      var promise = (gameBadge.id)? gameBadgeFactory.update(gameBadge) : gameBadgeFactory.save(gameBadge);

      promise.then(function (response) {

        // lembrar de setar o id para o objeto gameBadge
        gameBadge.id = response.data.id;

        toastr.success($scope.labels.messages.toast.badges.badgeWasSaved.text, $scope.labels.messages.toast.badges.badgeWasSaved.title);

        $scope.isBadgeProcessing = false;

      }).catch(function (response) {
        // isso é um unprocessable entity
        if (response.status == 422) {
          var attribute = Object.keys(response.data.errors)[0];
          var values = response.data.errors[attribute]; // vem em array cada conjunto de erros do rails (ou angular, sei lá)
          values.forEach(function (e, i, a) {
            toastr.error(e, $scope.labels.messages.toast.badges.badgeWasNotSaved.title);
          });
        } else {
          toastr.error($scope.labels.messages.toast.badges.badgeWasNotSaved.text, $scope.labels.messages.toast.badges.badgeWasNotSaved.title);
        }

        $scope.isBadgeProcessing = false;
      });
    }

    // ############ REMOVER BADGE
    $scope.delete = function (gameBadge) {

      $scope.isBadgeProcessing = true;

      gameBadgeFactory.delete(gameBadge).then(function () {
        // remover aqui a badge da lista
        $scope.gameBadges.find(function (e, i, a) {
          if (e && e.id == gameBadge.id) {
            $scope.gameBadges.splice(i, 1);
            toastr.success($scope.labels.messages.toast.badges.badgeWasDeleted.text, $scope.labels.messages.toast.badges.badgeWasDeleted.title);

            $scope.gameBadge = undefined; // não tem mais a badge selecionada
            $scope.isBadgeProcessing = false;
          }
        })
      }).catch(function (errors) {
        toastr.error($scope.labels.messages.toast.badges.badgeWasNotDeleted.text, $scope.labels.messages.toast.badges.badgeWasNotDeleted.title);

        $scope.isBadgeProcessing = false;
      });
    }
   }];
   
   /**
    * Faz bind do evento change para upload de arquivos
    *
    * @namespace game.controllers
    * @class gameBadgeImageSelect
    * @constructor
    */
   var gameBadgeImageSelect = function () {
     return {
       restrict: 'A',
       link: function(scope, element, attrs){
         // se eu selecionar o mesmo arquivo em duas badges diferentes, terá problema #bug
         element.bind("change", function(e){
           scope.gameBadge.file = (e.srcElement || e.target).files[0];
           scope.getFile();
         });
       }   
     }
   };
  
  /**
   * Módulo de controllers de game badges
   * 
   * @module gamekeeper.game.badges.controllers
   */
  angular.module('gamekeeper.game.badges.controllers', [])
    .controller('GameBadgeCtrl', GameBadgeCtrl)
      .directive('gameBadgeImageSelect', gameBadgeImageSelect);
})();

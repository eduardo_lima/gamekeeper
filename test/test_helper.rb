ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...





  # Retorna o conteúdo da action em json e converte para hash
  #
  #
  #
  #
  def better_action_call(method, action, params = nil, format = 'json')
    case method
    when :get
      return JSON.parse(get(action, format: format).body)
    when :post
      return JSON.parse(post(action, params).body) 
    end
  end
end

[33mcommit dee317f651961b18a9011a402ca3b36ca01cc047[m
Author: ehclima <edu.licap@gmail.com>
Date:   Fri Oct 14 16:42:17 2016 -0300

    sem precompile

[1mdiff --git a/app/assets/javascripts/ng/app/app-config-routes.js b/app/assets/javascripts/ng/app/app-config-routes.js[m
[1mindex ec7588f..c8113ff 100644[m
[1m--- a/app/assets/javascripts/ng/app/app-config-routes.js[m
[1m+++ b/app/assets/javascripts/ng/app/app-config-routes.js[m
[36m@@ -511,3 +511,16 @@[m
    */[m
   angular.module('gamekeeper.routes', []).config(routesConfig)[m
 })();[m
[32m+[m
[32m+[m[32m// Rake::Task["assets:precompile"].clear[m
[32m+[m[32m//    namespace :assets do[m
[32m+[m[32m//      task 'precompile' do[m
[32m+[m[32m//      puts "Not pre-compiling assets..."[m
[32m+[m[32m//    end[m
[32m+[m[32m// end[m
[32m+[m
[32m+[m[32m// JS_PATH = "app/assets/javascripts/**/*.js";[m[41m [m
[32m+[m[32m// Dir[JS_PATH].each do |file_name|[m
[32m+[m[32m//   puts "\n#{file_name}"[m
[32m+[m[32m//   puts Uglifier.compile(File.read(file_name))[m
[32m+[m[32m// end[m
\ No newline at end of file[m
[1mdiff --git a/lib/tasks/assets.rake b/lib/tasks/assets.rake[m
[1mnew file mode 100644[m
[1mindex 0000000..ed43447[m
[1m--- /dev/null[m
[1m+++ b/lib/tasks/assets.rake[m
[36m@@ -0,0 +1,6 @@[m
[32m+[m[32mRake::Task["assets:precompile"].clear[m
[32m+[m[32m    namespace :assets do[m
[32m+[m[32m      task 'precompile' do[m
[32m+[m[32m      puts "Not pre-compiling assets..."[m
[32m+[m[32m    end[m
[32m+[m[32m end[m
\ No newline at end of file[m

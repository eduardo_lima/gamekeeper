(function () {

  /**
   * Classe de game factories, consiste em chamadas de backend e métodos auxiliares
   *
   * @namespace game.factories
   * @class gameFactory
   * @constructor
   * @param $http Serviço de requisições http
   * @param Upload Helper para upload de imagens
   */
  var gameFactory = ['$http', 'Upload', function ($http, Upload) {
    
    /**
     * Método para salvar os atributos do game
     *
     * @method _internalSave
     * @param game Objeto com os atributos do game
     * @param method Tipo de verbo HTTP (POST, PUT)
     * @param url Endpoint da aplicação de backend
     * @return {Object} Angular Promise
     */
    var _internalSave = function (game, method, url) {
      var image = game.file ? game.file: [];
      var options = {
        url: url,
        method: method,
        file: image,
        file_form_data_name: game.name ? game.name: "",
        fields: {
          game: game
        }
      }
      return Upload.upload(options);  
    }
    
    /**
     * Chama a aplicação back-end para salvar o game
     *
     * @method _save
     * @param game Objeto com os atributos do game
     * @return {Object} Angular Promise
     */
    var _save = function (game) {
      return _internalSave(game, 'POST', '/games.json');
    }

    /**
     * Chama a aplicação back-end para atualizar o game
     *
     * @method _save
     * @param game Objeto com os atributos do game
     * @return {Object} Angular Promise
     */
    var _update = function (game) {
      return _internalSave(game, 'PUT', '/games/' + game.id + '.json');
    }
    
    /**
     * Chama a aplicação back-end para remover o game
     *
     * @method _delete
     * @param game Objeto com os atributos do game
     * @return {Object} Angular Promise
     */
    var _delete = function (game) {
      return $http.delete('/games/' + game.unique_name + '.json');
    }
    
    /**
     * Chama a aplicação back-end para obter todos os games do usuário
     *
     * @method _list
     * @return {Object} Angular Promise
     */
    var _list = function () {
      return $http.get('/games.json');
    }
    
    var _find = function (uniqueName) {
      return $http.get('/games/' + uniqueName + '.json');
    }

    var _changeToken = function (game) {
      return $http.put('/games/' + game.unique_name + '/change_token.json');
    }
    
    return {
      save: _save,
      update: _update,
      changeToken: _changeToken,
      delete: _delete,
      list: _list,
      find: _find 
    }
    
  }];
  
    
  /**
   * Módulo de factories de game
   * 
   * @module game.factories
   */
  angular.module('gamekeeper.game.factories', ['ngFileUpload'])
    .factory('gameFactory', gameFactory);

})();

module ApplicationHelper
  # Tem que validar se usuário pode alterar o game, se é dele
  def current_user_is_owner?(game_id)
    # Achar o jogo que ele diz que é dele
    # Vamos ver se é verdade
    return @current_user.id == Game.find_by(id: game_id).user_id unless @current_user.nil?
  end

  # O mesma validação, porém com unique_name (que não é tão recomendável)
  def current_user_is_owner!(unique_name)
    # Achar o jogo que ele diz que é dele
    # Vamos ver se é verdade
    return @current_user.id == Game.find_by(unique_name: unique_name).user_id unless @current_user.nil?
  end
end

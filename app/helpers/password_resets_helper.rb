module PasswordResetsHelper

  def request_recover_instructions email
    GatekeeperService.new.send_recover_password_instructions(email)
  end

  def request_recover_password token, email, password, password_confirmation
    GatekeeperService.new.recover_password(token, email, password, password_confirmation)
  end

end

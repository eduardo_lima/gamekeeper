include ApplicationHelper

class LevelsController < ApplicationController
  # antes de qualquer ação, o usuário deverá estar logado
  before_action :require_user
  # antes de criar ou atualizar o level é preciso processar o parâmetro file
  before_filter :process_params, only: [:create, :update]
  # antes de criar, listar ou deletar é preciso verificar se o game pertence ao usuário
  before_action :correct_user, only: [:create, :update, :destroy, :index]
  before_filter :current_level, only: [:update, :destroy]
  before_filter :conjure_level_instance, only: [:create, :update]

  def index
    respond_with Level.where(game_id: params[:game_id], hidden: false, removed: false).collect { |l| l.to_hash(:basic) }
  end

  def create
    if @level.save
      respond_with do |format|
        format.json { render json: { :flash => "Game level #{@level.name} has been created.", :id => @level.id }, status: :ok }
      end
    else
      respond_with @level
    end
  end

  def update
    if @level.save
      respond_with do |format|
        format.json { render json: { :flash => "Game level #{@level.name} has been updated.", :id => @level.id }, status: :ok }
      end
    else
      respond_with @level
    end
  end

  def destroy
    if @level.remove_this
      respond_with do |format|
        format.json { render json: { :flash => "Game level #{@level.name} has been destroyed.", :id => @level.id },status: :ok }
      end
    else
      respond_with do |format|
        format.json { render json: { :flash => "Something went wrong when destroying game level."}, status: :internal_server_error }
      end
    end
  end

private

  # Carrega a game level que existe
  def current_level
    @level = Level.find(params[:id])
  end

  # Define o que pode ser permitido inserir na action
  def permitted_params
    params.require(:game_level)
      .permit(:game_id, :name, :required_points, :active, :image, :picture)
  end

  # Processa a imagem anexada
  def process_params
    params[:game_level] = JSON.parse(params[:game_level]).with_indifferent_access if hash.is_a?(String)

    if params[:file]
      params[:game_level][:image] = params[:file]
      params[:game_level][:picture] = params[:file]
    end
  end

  # Verificar se o usuário é autorizado a fazer isso
  def correct_user
    # Validar se o é mesmo dele
    unless current_user_is_owner?(params[:game_id])
      respond_with do |format|
        format.json { render json: { :flash => "This game doesn't belongs to you." }, status: :unauthorized }
      end
    end
  end

  # Preencher todos os campos do model level
  def conjure_level_instance
    # Verifica parametros permitidos
    params = permitted_params
    # Caso nao foi criado
    unless @level.present? then
      @level = Level.new
      @level.game_id = params[:game_id]
    end

    # Preenche os campos do modelo
    @level.name = params[:name]
    @level.required_points = params[:required_points]
    @level.active = params[:active]
    @level.image = params[:image]
    @level.picture = params[:picture]
  end
end

class Player < ActiveRecord::Base
  belongs_to :game
  belongs_to :level

  has_many :player_actions
  has_many :player_badges
  has_many :performed_actions

  # attr_accessor :level
  
  # uma player tem uma imagem de referencia
  has_attached_file :image, :styles => { :small => "150x150>" },
                  :default_url => "/assets/game-player-icon.png",
                  :url  => "/assets/game_players/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/game_players/:id/:style/:basename.:extension"

  mount_uploader :picture, PictureUploader
  
  # o anexo da imagem tem que ser uma imagem
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  # o id da player deve ser preenchido
  validates :game_id,
            :presence   => { :message => I18n.t('activerecord.player.errors.messages.game_id.presence') }
  # o identificador da player nao deve conter espacos
  validates :name,
            :presence   => { message: I18n.t('activerecord.player.errors.messages.name.presence')},
            :length     => { maximum: 50, message: I18n.t('activerecord.player.errors.messages.name.length')}

  # Incrementar contador de players no game
  after_create :increment_game_players
  after_create :perform_first_actions
  # Decrementar contadr de players no game
  after_destroy :decrement_game_players

  # def level
  #   Level.find self.level_id
  # end
  
  # Converte os campos do model de acordo com a opcao determinada.
  # Retorna um hash com os campos do model.
  #
  # O parametro 'inc' define qual serao os campos incluidos no 
  # hash. Quando 'inc' for <tt>:all</tt> todos os campos devem 
  # estar no hash.
  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) then
      hash[:id] = self.id
      hash[:game_id] = self.game_id
      hash[:name] = self.name
      hash[:points_obtained] = self.points_obtained
      hash[:image_url] = picture_url
      hash[:level_imagel_url] = self.level.picture.url.present?? self.level.picture.url : "game-level-icon.png"
      hash[:badges_obtained] = self.player_badges.count
    end

    if inc.include?(:all) or inc.include?(:details) then
      hash[:timeline] = []

      self.performed_actions.each do |pa|
        hash[:timeline] << {
          klass: pa.class.to_s,
          player_name: self.name,
          action_id: pa.action.id,
          created_at: pa.created_at.strftime('%d/%m/%Y %H:%M'),
          description: "#{pa.action.past_tense} #{pa.action.action_object.article} #{pa.action.action_object.singular} e ganhou #{pa.action.points} pontos.",
          properties: pa.performed_action_properties.collect { |pap| pap.to_hash(:basic) }
          
        }
      end

      self.player_badges.each do |b|
        hash[:timeline] << {
          klass: b.class.to_s,
          player_name: self.name,
          created_at: b.created_at.strftime('%d/%m/%Y %H:%M'),
          description: "ganhou #{b.badge.bonus_points} pontos ao conquistar a insígnia #{b.badge.name}."
        }
      end

      # # ordenar tudo por timeline
      hash[:timeline].sort! { |a,b| b[:created_at] <=> a[:created_at]}
    end

    if inc.include?(:all) or inc.include?(:stats) then
      hash[:id] = self.id
      hash[:name] = self.name
      hash[:social_id] = self.social_media_id
      hash[:points_obtained] = self.points_obtained
      hash[:level_name] = self.level.name
      hash[:level_photo] = self.level.picture.url
    end


    if inc.include?(:all) or inc.include?(:rewards) then
      hash[:id] = self.id
      hash[:name] = self.name
      hash[:social_id] = self.social_media_id
      hash[:points_obtained] = self.points_obtained
      hash[:level_name] = self.level.name
      hash[:level_photo] = self.level.picture.url.present?? self.level.picture.url : "game-player-icon.png"
      hash[:player_photo] = picture_url

      hash[:badges_obtained] = {
        :count => self.player_badges.count,
        :data => self.player_badges.collect { |pb| pb.to_hash(:api) }
      }
    end
    
    return hash
  end

private

  # Incrementa o número de players no game
  def increment_game_players
    game = Game.find self.game.id
    game.increment :number_of_players
    game.save
  end

  def perform_first_actions
    puts "perform_first_actions"
    puts self.inspect

    PerformedAction.new do |pa|
      pa.player = self
      pa.action = Action.first
    end.save
  end

  # Decrementa o número de player no game
  def decrement_game_players
    game = Game.find self.game.id
    game.decrement :number_of_players
    game.save
  end

  def picture_url
    return self.picture.url
  rescue DropboxError
    return "game-player-icon.png"
  end

end

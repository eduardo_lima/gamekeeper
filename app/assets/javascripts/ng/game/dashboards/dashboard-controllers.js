(function () {
  
  var GameDashboardCtrl = ['$scope', 'labelsFactory', 'gameDashboardFactory', 'toastr', 
    function ($scope, labelsFactory,gameDashboardFactory, toastr) {

    $scope.actions = [];
    $scope.badges = [];
    $scope.labels = labelsFactory.gameOverview();
    $scope.tab = 1;
    $scope.actionsLoading = true;
    $scope.badgesLoading = true;
    $scope.levelsLoading = true;
    
    // carregando ações
    gameDashboardFactory.loadActionReport($scope.game.id).then(function (results) {
      $scope.actions = results.data;
      $scope.actionsLoading = false;
    });

    // carregando insígnias
    gameDashboardFactory.loadBadgeReport($scope.game.id).then(function (results) {
      $scope.badges = results.data;

      $scope.badgesLoading = false;
    });

    // carregando níveis
    gameDashboardFactory.loadLevelReport($scope.game.id).then(function (results) {
      $scope.levels = results.data;

      $scope.levelsLoading = false;
    });

   }];
  
  angular.module('gamekeeper.game.dashboards.controllers', ['googlechart'])
    .controller('GameDashboardCtrl', GameDashboardCtrl);
})();

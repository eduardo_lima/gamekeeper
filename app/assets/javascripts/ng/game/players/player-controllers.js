(function () {
  
  var GamePlayerCtrl = ['$scope', 'labelsFactory', 'gamePlayerFactory', 'toastr',
    function ($scope, labelsFactory, gamePlayerFactory, toastr) {

    // carrega as labels dos componentes da html
    $scope.labels = labelsFactory.gamePlayers();
    // carrega as levelss cadastradas no game
    $scope.gamePlayers = []; // lembrar do serviço

    $scope.playersAreLoaded = false;
    
    gamePlayerFactory.start();

    // ############ ALTERAR PAGINA
    $scope.changePage = function (page) {

      $scope.playersAreLoaded = false;

      // pega tudo atualizado
      gamePlayerFactory.list($scope.game.id, { page: page }).then(function (results) {
        $scope.gamePlayers = results.data.players;
        $scope.pagesOfPlayers = []; 

        // calcular quantas paginas terao
        var pagesCount = Math.ceil(results.data.count / 10);
        for (i = 1; i <= pagesCount; i++) {
          $scope.pagesOfPlayers.push(i);
        };
        
        $scope.playersAreLoaded = true;
      }).catch(function () {
        toastr.error($scope.labels.messages.toast.players.playersWereNotLoaded.text, $scope.labels.messages.toast.players.playersWereNotLoaded.title);
        $scope.playersAreLoaded = true;
      });

    };

    $scope.changePage(1);

  }];

  var GameMenuPlayerCtrl = ['$scope', '$state', '$stateParams', 'menu', 'labels', 'miscLabels', 'game', 'player',
    function ($scope, $state, $stateParams, menu, labels, miscLabels, game, player) {
    
    $scope.id = $stateParams.id;
    // obtem labels especificas
    $scope.labels = labels;
    // obtem labels miscelaneas
    $scope.miscLabels = miscLabels;
    // obtem response com o projeto
    $scope.game = game.data;
    $scope.player = player.data;
    // Definir o menu selecionado
    $scope.selectedMenu = menu;

    $scope.isActiveMenu = function (supposedMenus) {
      return supposedMenus.indexOf($scope.selectedMenu) >= 0;
    }
  }];

  var GamePlayerDetailCtrl = ['$scope', 'labelsFactory', 'gamePlayerFactory', 'toastr', '$uibModal',
    function ($scope, labelsFactory, gamePlayerFactory, toastr, $uibModal) {

    // carrega as labels dos componentes da html
    $scope.labels = labelsFactory.gamePlayersDetail();

    $scope.playerIsLoaded = false;

    $scope.timeline = [];

    // ############ LISTAR atividades
    gamePlayerFactory.getPlayerDetail($scope.game.id, $scope.player.id).then(function (results) {
      $scope.timeline = results.data.timeline;

      $scope.playerIsLoaded = true;
    }).catch(function () {
      toastr.error($scope.labels.messages.toast.player.playerInfoWasNotLoaded.text, $scope.labels.messages.toast.player.playerInfoWasNotLoaded.title);
      $scope.playerIsLoaded = true; 
    });

    // ############ mostrar detalhes ação
    $scope.openActionInfo = function (properties) {
      
      $uibModal.open({
        animation: false,
        ariaLabelledBy: 'modal-title-top',
        ariaDescribedBy: 'modal-body-top',
        templateUrl: 'myModalContent.html',
        size: 'sm',
        resolve: {
          properties: function () {
            return properties;
          }
        },
        controller: ['$scope', 'properties', '$uibModalInstance', function($scope, properties, $uibModalInstance) {
          $scope.name = 'top';  
          $scope.properties = properties;

          $scope.cancel = function () {
            $scope.properties = [];
            $uibModalInstance.dismiss('cancel');
          };
        }]
      });
    };
  }];

  var GamePlayerSandboxCtrl = ['$scope', 'labelsFactory', 'gameActionFactory', 'gamePlayerFactory', 'toastr',
    function ($scope, labelsFactory, gameActionFactory, gamePlayerFactory, toastr) {

    // carrega as labels dos componentes da html
    $scope.labels = labelsFactory.gamePlayersSandbox();

    $scope.action = {};
    $scope.object_properties = [];

    $scope.actionLoading = true;

    gameActionFactory.start();
    gameActionFactory.list($scope.game.id).then(function (results) {
      $scope.gameActions = results.data;

      $scope.actionLoading = false;
    }).catch(function () {
      toastr.error($scope.labels.messages.toast.sandbox.sandboxWasNotLoaded.text, $scope.labels.messages.toast.sandbox.sandboxWasNotLoaded.title);
      $scope.actionLoading = false;
    });

    $scope.updateProperties = function () {

      $scope.object_properties = [];

      $scope.object_properties = $scope.action.object_properties.map(function (prop) {
        return {
          name: prop.name,
          value: undefined
        }
      });
    };

    $scope.perform = function () {

      $scope.actionPerforming = true;

      var performedAction = {}

      performedAction.verb = $scope.action.verb;
      performedAction.object_properties = $scope.object_properties;

      gamePlayerFactory.performAction($scope.game.id, $scope.player.id, performedAction).then(function (result) {
        toastr.success($scope.labels.messages.toast.sandbox.actionPerformed.text, $scope.labels.messages.toast.sandbox.actionPerformed.title);

        $scope.action = {};
        $scope.object_properties = [];

        $scope.actionPerforming = false;
      }).catch(function () {
        toastr.error($scope.labels.messages.toast.sandbox.actionNotPerformed.text, $scope.labels.messages.toast.sandbox.actionNotPerformed.title);
        $scope.actionPerforming = false;
      });
      
    }
  }];
  
  angular.module('gamekeeper.game.players.controllers', [])
    .controller('GamePlayerCtrl', GamePlayerCtrl)
    .controller('GameMenuPlayerCtrl', GameMenuPlayerCtrl)
    .controller('GamePlayerDetailCtrl', GamePlayerDetailCtrl)
    .controller('GamePlayerSandboxCtrl', GamePlayerSandboxCtrl);
})();

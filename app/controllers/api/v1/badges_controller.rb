class API::V1::BadgesController < ApiController

  before_action :load_saved_game, :init_hash

  def index
    badges = Badge.where(game_id: @game.id, active: true)

    @hash[:data] = badges.present?? badges.collect { |b| b.to_hash(:api)} : []

    respond_with @hash
  end

  def show
    badge = Badge.find_by_game_id_and_id_and_active(@game.id, params[:badge_id], true)

    @hash[:data] = badge.present?? badge.to_hash(:api) : []

    respond_with @hash
  end

private

  def init_hash
    @hash = {}
  end

  def load_saved_game
    @game = Game.find_by_unique_name(params[:unique_name])
  end
end
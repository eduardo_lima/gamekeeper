class ObjectProperty < ActiveRecord::Base
  # uma property pertence a uma ação
  belongs_to :action_object

  # o nome da propriedade da action é obrigatoria
  VALID_NAME_REGEX = /\A[a-z0-9\-_]+\z/
  validates :name,
            :presence   => { :message => I18n.t('activerecord.object_property.errors.messages.name.presence') },
            :format     => { with: VALID_NAME_REGEX, message: I18n.t('activerecord.object_property.errors.messages.name.format') },
            :length     => { :maximum => 50, :message => I18n.t('activerecord.object_property.errors.message.name.length') }
  # o tipo da propriedade da action é obrigatória
  # validates :kind,
  #           :presence   => { :message => I18n.t('activerecord.object_property.errors.message.kind.presence') },
  #           :length     => { :maximum => 50, :message => I18n.t('activerecord.object_property.errors.message.kind.length') }


  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}

    if inc.include?(:all) or inc.include?(:api) then
      hash[:name] = self.name
      hash[:kind] = self.kind
    end
    
    return hash
  end

  def remove_this
    self.removed = true
    self.save
  end
end

include ApplicationHelper

class BadgesController < ApplicationController
  # antes de qualquer ação, o usuário deverá estar logado
  before_action :require_user
  # antes de criar ou atualizar o badge é preciso processar o parâmetro file
  before_filter :process_params, only: [:create, :update]
  # antes de criar, listar ou deletar é preciso verificar se o game pertence ao usuário
  before_action :correct_user, only: [:create, :update, :destroy, :index]
  before_filter :current_badge, only: [:update, :destroy]
  before_filter :conjure_badge_instance, only: [:create, :update]

  # Traz todos os badges
  def index
    respond_with Badge.where(game_id: params[:game_id], removed: false).collect { |b| b.to_hash(:basic) }
  end

  # Cria uma nova badge
  def create
    if @badge.save
      respond_with do |format|
        format.json { render json: { :flash => "Game badge #{@badge.name} has been created.", :id => @badge.id }, status: :ok }
      end
    else
      respond_with @badge
    end
  end

  # Atualiza a game badge
  def update
    if @badge.save
      respond_with do |format|
        format.json { render json: { :flash => "Game badge #{@badge.name} has been updated.", :id => @badge.id }, status: :ok }
      end
    else
      respond_with @badge
    end
  end

  # Remove a game badge
  def destroy
    if @badge.remove_this
      respond_with do |format|
        format.json { render json: { :flash => "Game badge #{@badge.name} has been destroyed.", :id => @badge.id },status: :ok }
      end
    else
      respond_with do |format|
        format.json { render json: { :flash => "Something went wrong when destroying game badge."}, status: :internal_server_error }
      end
    end
  end

private

  # Carrega a game badge que existe
  def current_badge
    @badge = Badge.find(params[:id])
  end

  # Define o que pode ser permitido inserir na action
  def permitted_params
    params.require(:game_badge)
      .permit(:game_id, :name, :bonus_points, :active, :image, :picture,
              :conditions_attributes => [:condition_type, :badge_reward_actions],
              :badge_reward_actions => [:verb, :times_to_achieve])
  end

  # Processa a imagem anexada
  def process_params
    params[:game_badge] = JSON.parse(params[:game_badge]).with_indifferent_access if hash.is_a?(String)

    if params[:file]
      params[:game_badge][:image] = params[:file]
      params[:game_badge][:picture] = params[:file]
    end
  end

  # Verificar se o usuário é autorizado a fazer isso
  def correct_user
    # Validar se o é mesmo dele
    unless current_user_is_owner?(params[:game_id])
      respond_with do |format|
        format.json { render json: { :flash => "This game doesn't belongs to you." }, status: :unauthorized }
      end
    end
  end

  # Preencher todos os campos do model badge
  def conjure_badge_instance
    # Verifica parametros permitidos
    params = permitted_params
    # Caso nao foi criado
    unless @badge.present? then
      @badge = Badge.new
      @badge.game_id = params[:game_id]
    end

    # Preenche os campos do modelo
    @badge.name = params[:name]
    @badge.bonus_points = params[:bonus_points]
    @badge.active = params[:active]
    @badge.image = params[:image]
    @badge.picture = params[:image]
    @badge.badge_reward_condition = BadgeRewardCondition.new unless @badge.badge_reward_condition.present?

    @badge.badge_reward_condition.condition_type = params[:conditions_attributes][:condition_type]

    @badge.badge_reward_condition.badge_reward_actions.map(&:remove_this)
    params[:badge_reward_actions].each do |key, action|
      bra = BadgeRewardAction.new
      bra.action = Action.find_by(game_id: params[:game_id], verb: action['verb'])
      bra.times_to_achieve = action['times_to_achieve']
      @badge.badge_reward_condition.badge_reward_actions << bra
    end if params[:badge_reward_actions].present?

  end


end

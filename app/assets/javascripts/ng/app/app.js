(function () {
  
  /**
   * Módulo principal da aplicação front-end
   * 
   * @module gamekeeper
   * @requires ui.router Módulo das rotas disponíveis da aplicação
   * @requires templates ...
   * @requires ngSanitize Módulo para escrever expressões JS no html
   * @requires ngMessages Diretiva para mensagens de validação/erro no html
   * @requires ngAnimate Diretiva para animação
   * @requires ngBootbox Criador de caixas de dialogo
   * @requires toastr Módulo de mensagens de toast
   * @requires ui.bootstrap Coleção de diretivas de componentes html bootstrap
   * @requires gamekeeper.config Módulo que contém as configurações da aplicação
   * @requires sessions Módulo para controlar as sessões de usuário
   * @requires game Módulo principal de sistemas gamififcados
   * @requires users Módulo de usuários
   * @requires utils Etc
   */
  angular.module('gamekeeper', [
    'ui.router',
    'templates',
    'ngSanitize',
    'ngMessages',
    'ngAnimate',
    'ngBootbox',
    'toastr',
    'ui.bootstrap',
    'gamekeeper.config',
    'gamekeeper.sessions',
    'gamekeeper.game',
    'gamekeeper.users',
    'gamekeeper.utils'
  ])

})();
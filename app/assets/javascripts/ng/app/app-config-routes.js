(function () {

  /**
   * Configura os states da biblioteca ui-router.
   * @memberof gamekeeper.routes
   * @ngdoc config
   * @name routesConfig
   * @param {Service} $stateProvider Disponibiliza a interface para os states da aplicação
   * @param {Service} $urlRouterProvider Redireciona para url padrão
   */
  var routesConfig = ['$stateProvider', '$urlRouterProvider', 
    function($stateProvider, $urlRouterProvider) {

    /**
     * @memberof routesConfig
     * @description
     *  State inicial da aplicação
     */
    $stateProvider.state('app', {
      abstract: true,
      template: '<div ui-view></div>'
    });

    /**
     * @memberof routesConfig
     * @description
     *  State inicial para o usuário
     */
    $stateProvider.state('app.user', {
      abstract: true,
      templateUrl: 'assets/views/users/_layout.html'
    });


    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------


    /**
     * @memberof routesConfig
     * @description
     *  State de criação de contas de usuário
     *
     *  Responde quando a url relativa é signup
     *
     *  Tem dependência com a factory de labels para renderiração
     */
    $stateProvider.state('app.user.signup', {
      url: '/signup',
      templateUrl: 'assets/views/users/_signup.html',
      controller: 'SignupCtrl',
      resolve: {
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.signup();
        }]
      }
    });

    /**
     * @memberof routesConfig
     * @description
     *  State de de confirmação de envio de email de cadastro
     *
     *  Responde quando a url relativa é confirmation
     */
    $stateProvider.state('app.user.signupConfirmation', {
      url: '/confirmation',
      templateUrl: 'assets/views/users/_activation-info.html'
    });

    /**
     * @memberof routesConfig
     * @description
     *  State de de confirmação de envio de email de cadastro
     *
     *  token Código de ativação enviado no email
     *  email Email do usuário cadastrado
     *
     *  Responde quando a url relativa é activation

     */
    $stateProvider.state('app.user.signupActivation', {
      url: '/activation?token&email',
      templateUrl: 'assets/views/users/_activation-form.html',
      controller: 'ActivationCtrl',
    });


    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------


    /**
     * @memberof routesConfig
     * @description
     *  State de autenticação do usuário
     *
     *  Responde quando a url relativa é login
     *
     *  Tem dependência com a factory de labels para renderização
     */
    $stateProvider.state('app.user.login', {
      url: '/login',
      templateUrl: 'assets/views/users/_login.html',
      controller: 'LoginCtrl',
      resolve: {
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.login();
        }]
      }
    });


    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------

    /**
     * @memberof routesConfig
     * @description
     *  State que renderiza formulário para enviar código de reset de senha
     *
     *  Responde quando a url relativa é send_recover_password
     *
     *  Tem dependência com a factory de labels para renderização
     */
    $stateProvider.state('app.user.passwordRecoverSender', {
      url: '/send_recover_password',
      templateUrl: 'assets/views/users/_recover-email-form.html',
      controller: 'PasswordRecoverSenderCtrl',
      resolve: {
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.passwordRecoverSender();
        }]
      }
    });

    /**
     * @memberof routesConfig
     * @description
     *  State que renderiza confirmação de código de reset enviado
     *
     *  Responde quando a url relativa é recover_password_info
     *
     *  Tem dependência com a factory de labels para renderização
     */
    $stateProvider.state('app.user.passwordRecoverEmail', {
      url: '/recover_password_info',
      templateUrl: 'assets/views/users/_recover-password-info.html'
    });

    /**
     * @memberof routesConfig
     * @description
     *  State de reset de senha de usuário
     *
     *  token Código de reset de email
     *  email Email para reset de senha
     *
     *  Responde quando a url relativa é recover_password
     *
     *  Tem dependência com a factory de labels para renderização
     */
    $stateProvider.state('app.user.passwordRecover', {
      url: '/recover_password?token&email',
      templateUrl: 'assets/views/users/_recover-password-form.html',
      controller: 'PasswordRecoverFormCtrl',
      resolve: {
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.passwordRecoverForm();
        }]
      }
    });


    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------


    /**
     * @memberof routesConfig
     * @description
     *  State da página de edição do usuário
     *
     *  Responde quando a url relativa é edit
     *
     *  Tem dependência com a factory de labels para renderização
     */
    $stateProvider.state('app.user.edit', {
      url: '/edit',
      templateUrl: 'assets/views/users/_edit.html',
      controller: 'EditAccountCtrl',
      resolve: {
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.editAccount();
        }],
        miscLabels: ['labelsFactory', function(labelsFactory) {
          return labelsFactory.shared();
        }]
      }
    });


    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------


    $stateProvider.state('app.game', {
      abstract: true,
      url: '/games',
      templateUrl: 'assets/views/games/_layout.html'
    });

    /**
     * @memberof routesConfig
     * @description
     *  State da página inicial do usuário
     *
     *  Responde quando a url relativa é games
     *
     *  Tem dependência com a factory de labels para renderização
     */
    $stateProvider.state('app.game.list', {
      url: '',
      templateUrl: 'assets/views/games/_game-list.html',
      controller: 'GameListCtrl',
      resolve: {
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gameList();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }]
      }
    });

    /**
     * @memberof routesConfig
     * @description
     *  State da página de overview do game
     *
     *  Dependencias
     *  menu: retorna o menu selecionado ao entrar neste state
     *  labels: retorna labels específicas para o game overview
     *  miscLabels: retorna labels gerais do projeto
     *  game: retorna as informações do game
     */
    $stateProvider.state('app.game.overview', {
      url: '/:id/overview',
      templateUrl: 'assets/views/games/_overview.html',
      controller: 'GameMenuCtrl',
      resolve: {
        menu: [function () {
          return 'overview';
        }],
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gameOverview();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }],
        game: ['$stateParams', 'gameFactory', function ($stateParams, gameFactory) {
          return gameFactory.find($stateParams.id);
        }],
        user: ['sessionFactory', function (sessionFactory) {
          return sessionFactory.loggedIn();
        }]
      }
    });
    
    /**
     * @memberof routesConfig
     * @description
     *  State da página de players do game
     *
     *  Dependencias
     *  menu: retorna o menu selecionado ao entrar neste state
     *  labels: retorna labels específicas para o game players
     *  miscLabels: retorna labels gerais do projeto
     *  game: retorna as informações do game
     */
    $stateProvider.state('app.game.players', {
      url: '/:id/players',
      templateUrl: 'assets/views/games/_players.html',
      controller: 'GameMenuCtrl',
      resolve: {
        menu: [function () {
          return 'players';
        }],
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gamePlayers();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }],
        game: ['$stateParams', 'gameFactory', function ($stateParams, gameFactory) {
          return gameFactory.find($stateParams.id);
        }],
        user: ['sessionFactory', function (sessionFactory) {
          return sessionFactory.loggedIn();
        }]
      }
    });

    /**
     * @memberof routesConfig
     * @description
     *  State da página de players do game
     *
     *  Dependencias
     *  menu: retorna o menu selecionado ao entrar neste state
     *  labels: retorna labels específicas para o game players
     *  miscLabels: retorna labels gerais do projeto
     *  game: retorna as informações do game
     */
    $stateProvider.state('app.game.players_detail', {
      url: '/:id/players/:player/show',
      templateUrl: 'assets/views/games/_players-detail.html',
      controller: 'GameMenuPlayerCtrl',
      resolve: {
        menu: [function () {
          return 'players';
        }],
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gamePlayersDetail();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }],
        game: ['$stateParams', 'gameFactory', function ($stateParams, gameFactory) {
          return gameFactory.find($stateParams.id);
        }],
        player: ['$stateParams', 'gamePlayerFactory', function ($stateParams, gamePlayerFactory) {
          return gamePlayerFactory.find($stateParams.id, $stateParams.player);
        }]
      }
    });

    $stateProvider.state('app.game.player_sandbox', {
      url: '/:id/players/:player/sandbox',
      templateUrl: 'assets/views/games/_players-sandbox.html',
      controller: 'GameMenuPlayerCtrl',
      resolve: {
        menu: [function () {
          return 'players';
        }],
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gamePlayersSandbox();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }],
        game: ['$stateParams', 'gameFactory', function ($stateParams, gameFactory) {
          return gameFactory.find($stateParams.id);
        }],
        player: ['$stateParams', 'gamePlayerFactory', function ($stateParams, gamePlayerFactory) {
          return gamePlayerFactory.find($stateParams.id, $stateParams.player);
        }]
      }
    });


    
    /**
     * @memberof routesConfig
     * @description
     *  State da página de actions do game
     *
     *  Dependencias
     *  menu: retorna o menu selecionado ao entrar neste state
     *  labels: retorna labels específicas para o game actions
     *  miscLabels: retorna labels gerais do projeto
     *  game: retorna as informações do game
     */
    $stateProvider.state('app.game.actions', {
      url: '/:id/actions',
      templateUrl: 'assets/views/games/_actions.html',
      controller: 'GameMenuCtrl',
      resolve: {
        menu: [function () {
          return 'actions';
        }],
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gameActions();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }],
        game: ['$stateParams', 'gameFactory', function ($stateParams, gameFactory) {
          return gameFactory.find($stateParams.id);
        }],
        user: ['sessionFactory', function (sessionFactory) {
          return sessionFactory.loggedIn();
        }]
      }
    });
    
    /**
     * @memberof routesConfig
     * @description
     *  State da página de badges do game
     *
     *  Dependencias
     *  menu: retorna o menu selecionado ao entrar neste state
     *  labels: retorna labels específicas para o game players
     *  miscLabels: retorna labels gerais do projeto
     *  game: retorna as informações do game
     */
    $stateProvider.state('app.game.badges', {
      url: '/:id/badges',
      templateUrl: 'assets/views/games/_badges.html',
      controller: 'GameMenuCtrl',
      resolve: {
        menu: [function () {
          return 'badges';
        }],
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gameBadges();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }],
        game: ['$stateParams', 'gameFactory', function ($stateParams, gameFactory) {
          return gameFactory.find($stateParams.id);
        }],
        user: ['sessionFactory', function (sessionFactory) {
          return sessionFactory.loggedIn();
        }]
      }
    });
    
    /**
     * @memberof routesConfig
     * @description
     *  State da página de levels do game
     *
     *  Dependencias
     *  menu: retorna o menu selecionado ao entrar neste state
     *  labels: retorna labels específicas para o levels players
     *  miscLabels: retorna labels gerais do projeto
     *  game: retorna as informações do game
     */
    $stateProvider.state('app.game.levels', {
      url: '/:id/levels',
      templateUrl: 'assets/views/games/_levels.html',
      controller: 'GameMenuCtrl',
      resolve: {
        menu: [function () {
          return 'levels';
        }],
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gameLevels();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }],
        game: ['$stateParams', 'gameFactory', function ($stateParams, gameFactory) {
          return gameFactory.find($stateParams.id);
        }],
        user: ['sessionFactory', function (sessionFactory) {
          return sessionFactory.loggedIn();
        }]
      }
    });
    
    /**
     * @memberof routesConfig
     * @description
     *  State da página de settings do game
     *
     *  Dependencias
     *  menu: retorna o menu selecionado ao entrar neste state
     *  labels: retorna labels específicas para o game settings
     *  miscLabels: retorna labels gerais do projeto
     *  game: retorna as informações do game
     */
    $stateProvider.state('app.game.settings', {
      url: '/:id/settings',
      templateUrl: 'assets/views/games/_settings.html',
      controller: 'GameMenuCtrl',
      resolve: {
        menu: [function () {
          return 'settings';
        }],
        labels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.gameSettings();
        }],
        miscLabels: ['labelsFactory', function (labelsFactory) {
          return labelsFactory.shared();
        }],
        game: ['$stateParams', 'gameFactory', function ($stateParams, gameFactory) {
          return gameFactory.find($stateParams.id);
        }],
        user: ['sessionFactory', function (sessionFactory) {
          return sessionFactory.loggedIn();
        }]
      }
    });

    // Rota padrão
    $urlRouterProvider.otherwise('games');
  }];

  /**
   * Módulo de configuração dos states da aplicacão
   *
   * @module gamekeeper.routes
   */
  angular.module('gamekeeper.routes', []).config(routesConfig)
})();

// Rake::Task["assets:precompile"].clear
//    namespace :assets do
//      task 'precompile' do
//      puts "Not pre-compiling assets..."
//    end
// end

// JS_PATH = "app/assets/javascripts/**/*.js"; 
// Dir[JS_PATH].each do |file_name|
//   puts "\n#{file_name}"
//   puts Uglifier.compile(File.read(file_name))
// end
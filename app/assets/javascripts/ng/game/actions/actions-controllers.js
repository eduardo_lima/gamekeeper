(function () {
  
  var GameActionCtrl = ['$scope', 'fileReader', 'labelsFactory', 'gameActionFactory', 'toastr', 'serialGenerator', 
    function ($scope, fileReader, labelsFactory, gameActionFactory, toastr, serialGenerator) {

    // carrega as labels dos componentes da html
    $scope.labels = labelsFactory.gameActions();
    // carrega as opções dos artigos do objeto
    $scope.articles = ['um', 'uma', 'o', 'a'];  // fazer factories pra isso - colocar promises encadeadas (para carregar tudo que [e necessario primeiro])
    // carrega as opções das propriedades do objeto
    $scope.propertyOptions = ["String", "Url"]; // fazer factories pra isso
    // carrega as actions cadastradas no game
    $scope.gameActions = []; // lembrar do serviço

    $scope.actionsAreLoaded = false;
    
    gameActionFactory.start();
    
    gameActionFactory.list($scope.game.id).then(function (results) {
      $scope.gameActions = results.data;

      $scope.actionsAreLoaded = true;
    });

    $scope.new = function () {
      // aqui será configurado de como a action será representada no form
      $scope.gameAction = {
        // não tem como ter id agora (undefined pq é falso ;)
        id: undefined,
        // quando é nova action então é pra estar selecionado
        isSelected: true,
        // atribui o game selecionado
        game_id: $scope.game.id,
        // inicializar a imagem
        image_url: '/assets/game-action-icon.png',
        // Valor do identificador
        identifier: '',
        // Tempo do verbo da action
        past_tense: '',
        // Pontos
        points: '',
        // pontos de bonus
        bonus_points: '',
        // objeto que será usado na ação
        action_object: {
          // artigo que define o objeto
          article: '',
          // nome do objeto no singular
          singular: '',
          // nome do objeto no plural
          plural: ''
        },
        // inicializar as Properties
        object_properties: []
      };

      // iterar e desselecionar tudo
      $scope.gameActions.find(function (e, i, a) {
        e.isSelected = false;
      })

      $scope.gameActions.push($scope.gameAction);
    }
      
    $scope.select = function (gameAction) {
      // iterar e desselecionar tudo
      $scope.gameActions.find(function (e, i, a) {
        e.isSelected = false;
      })

      // selecionar agora =)
      $scope.gameAction = gameAction;
      $scope.gameAction.isSelected = true;

      if (!$scope.gameAction.object_properties) {
        $scope.gameAction.object_properties = [];
      }

      // gerar serial pra melhor controlar o array
      $scope.gameAction.object_properties.find(function (e, i, a) {
        e.serial = serialGenerator.generate();
      });
    }

    $scope.getFile = function () {
        fileReader.readAsDataUrl($scope.gameAction.file, $scope).then(function(result) {
          $scope.gameAction.image_url = result;
        });
      };

    $scope.selectArticle = function (option) {
      $scope.gameAction.action_object.article = option;
    }

    $scope.addProperty = function () {
      if ($scope.gameAction.object_properties) {
        $scope.gameAction.object_properties.push({ name: "", kind: "", serial: serialGenerator.generate() });  
      } else {
        $scope.gameAction.object_properties = []
      } 
    }

    $scope.removeProperty = function (property) {
      var indexToRemove = undefined;
      $scope.gameAction.object_properties.find(function (e, i, a) {
        if (e.serial == property.serial) {
          indexToRemove = i;
        }
      });
      $scope.gameAction.object_properties.splice(indexToRemove, 1);
    }

    $scope.save = function (gameAction) {

      $scope.isActionProcessing = true;

      // ver se é edição ou criação, colocar condição aqui
      var promise = (gameAction.id)? gameActionFactory.update(gameAction) : gameActionFactory.save(gameAction);

      promise.then(function (response) {

        // lembrar de setar o id para o objeto gameAction
        gameAction.id = response.data.id;

        toastr.success($scope.labels.messages.toast.actions.actionWasSaved.text, $scope.labels.messages.toast.actions.actionWasSaved.title);

        $scope.isActionProcessing = false;

      }).catch(function (response) {
        // isso é um unprocessable entity
        if (response.status == 422) {
          var attribute = Object.keys(response.data.errors)[0];
          var values = response.data.errors[attribute]; // vem em array cada conjunto de erros do rails (ou angular, sei lá)
          values.forEach(function (e, i, a) {
            toastr.error(e, $scope.labels.messages.toast.actions.actionWasNotSaved.title);
          });
        } else {
          toastr.error($scope.labels.messages.toast.actions.actionWasNotSaved.text, $scope.labels.messages.toast.actions.actionWasNotSaved.title);
        }

        $scope.isActionProcessing = false;
      });
    }

    $scope.delete = function (gameAction) {

      $scope.isActionProcessing = true;

      gameActionFactory.delete(gameAction).then(function () {
        // remover aqui a action da lista
        $scope.gameActions.find(function (e, i, a) {
          if (e && e.id == gameAction.id) {
            $scope.gameActions.splice(i, 1);
            toastr.success($scope.labels.messages.toast.actions.actionWasDeleted.text, $scope.labels.messages.toast.actions.actionWasDeleted.title);

            $scope.gameAction = undefined; // não tem mais a action selecionada
            $scope.isActionProcessing = false;
          }
        })
      }).catch(function (errors) {
        toastr.error($scope.labels.messages.toast.actions.actionWasNotDeleted.text, $scope.labels.messages.toast.actions.actionWasNotDeleted.title);

        $scope.isActionProcessing = false;
      });
    }
   }];

   /**
    * Faz bind do evento change para upload de arquivos
    *
    * @namespace game.controllers
    * @class gameActionImageSelect
    * @constructor
    */
   var gameActionImageSelect = function () {
     return {
       restrict: 'A',
       link: function(scope, element, attrs){
         // se eu selecionar o mesmo arquivo em duas actions diferentes, terá problema #bug
         element.bind("change", function(e){
           scope.gameAction.file = (e.srcElement || e.target).files[0];
           scope.getFile();
         });
       }   
     }
   };
  
  /**
   * Módulo de controllers de game actions
   * 
   * @module gamekeeper.game.actions.controllers
   */
  angular.module('gamekeeper.game.actions.controllers', [])
    .controller('GameActionCtrl', GameActionCtrl)
      .directive('gameActionImageSelect', gameActionImageSelect);
})();

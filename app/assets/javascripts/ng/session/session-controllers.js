(function () {

  /**
   * Controller para as sessions do usuário
   *
   * @namespace session.controllers
   * @class LoginCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $state Helper para controlar states
   * @param sessionFactory Factory de chamadas de backend
   * @param labels Labels para os componentes da página
   * @param toastr Mensagens de feedback toast
   */
  var LoginCtrl = ['$scope', '$state', 'sessionFactory', 'labels', 'toastr',
    function ($scope, $state, sessionFactory, labels, toastr) {
    $scope.labels = labels;

    // método ao submitar o formulário, método sem parâmetro pois pega do escopo
    $scope.login = function (user) {

      $scope.isLoggingIn = true;

      sessionFactory.login({
        email: user.email,
        password: user.password,
        remember_me: user.remember
      }).then(function (response) {

        $scope.user = {};

        toastr.success($scope.labels.messages.toast.userWasLoggedIn.text, $scope.labels.messages.toast.userWasLoggedIn.title);

        $state.go('app.game.list');

      }).catch(function (response) {

        toastr.error(response.data.message, $scope.labels.messages.toast.userWasNotLoggedIn.title);

        $scope.isLoggingIn = false;
      })
    }
  }];
  
      
  /**
   * Módulo de controllers de sessions
   * 
   * @module session.controllers
   */
  angular.module('gamekeeper.session.controllers', [])
    .controller('LoginCtrl', LoginCtrl);

})();

include ApplicationHelper

class PlayersController < ApplicationController
  # antes de qualquer ação, o usuário deverá estar logado
  before_action :require_user
  # antes de criar ou atualizar o level é preciso processar o parâmetro file
  before_filter :process_params, only: [:create, :update]
  # antes de criar, listar ou deletar é preciso verificar se o game pertence ao usuário
  before_action :correct_user, only: [:index]
  before_action :correct_user!, only: [:show]
  
  def index
    page = 0
    per_page = 10

    page = params[:page].to_i - 1 if params.has_key? :page
    per_page = params[:per_page] if params.has_key? :per_page

    players = Player
              .where(game_id: params[:game_id], removed: false)
              .includes(:level, :player_badges)
              .offset(page * 10)
              .limit(per_page)
              .order('points_obtained DESC')

    hash = players.collect { |p| p.to_hash(:basic) }

    render json: { players: hash, count: Player.where(game_id: params[:game_id]).count } 
  end

  def show
    respond_with Player.find(params[:id]).to_hash(:basic)
  end

  def detail
    respond_with Player.find(params[:id]).to_hash(:details)
  end

  def perform_some_action

    # {"performed_action"=>{"verb"=>"compartilhar", 
    # "object_properties"=>[{"name"=>"Autor"}, {"name"=>"Plataforma"}]},  "game_id"=>"1", "id"=>"1", "format"=>"json", }
    performed_action = PerformedAction.new
    performed_action.player_id = params[:id]
    performed_action.action_id = Action.find_by(game_id: params[:game_id], verb: params[:performed_action][:verb]).id
    performed_action.save
    
    params[:performed_action][:object_properties].each do |property|
      next if property[:value].blank?

      performed_action_property = PerformedActionProperty.new
      performed_action_property.performed_action_id = performed_action.id
      performed_action_property.object_property_id = ObjectProperty.find_by(name: property[:name]).id
      performed_action_property.value = property[:value]
      performed_action.performed_action_properties << performed_action_property
    end if params[:performed_action][:object_properties].present?
 
    if performed_action.save
      respond_with do |format|
        format.json { render json: { :flash => "Performed action was created." },status: :ok }
      end
    else
      respond_with performed_action
    end
  end

private

  # Verificar se o usuário é autorizado a fazer isso
  def correct_user
    # Validar se o é mesmo dele
    unless current_user_is_owner?(params[:game_id])
      respond_with do |format|
        format.json { render json: { :flash => "This game doesn't belongs to you." }, status: :unauthorized }
      end
    end
  end

  def correct_user!
    # Validar se o é mesmo dele
    unless current_user_is_owner!(params[:game_id])
      respond_with do |format|
        format.json { render json: { :flash => "This game doesn't belongs to you." }, status: :unauthorized }
      end
    end
  end

end


PUBLIC_SCHEMAS = [
  'games'
].freeze

PRIVATE_SCHEMAS = [
  'actions', 
  'action_objects', 
  'object_properties'
].freeze
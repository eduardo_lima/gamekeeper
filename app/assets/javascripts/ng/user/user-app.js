(function () {
  
  /**
   * Módulo de usuário
   *
   * @module users
   * @requires users.controllers Módulo com controllers
   * @requires users.factories Módulo com factories
   */
  angular.module('gamekeeper.users', [
    'gamekeeper.user.controllers', 
    'gamekeeper.user.factories'
  ]);
  
})();



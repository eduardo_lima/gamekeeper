(function () {
  
  /**
   * Módulo de configurações da aplicação
   * 
   * @module gamekeeper.config
   * @requires gamekeeper.routes Configuração de rotas/states da aplicação
   */
  angular.module('gamekeeper.config', [
    'gamekeeper.routes',
    'gamekeeper.labels'
  ])

})();


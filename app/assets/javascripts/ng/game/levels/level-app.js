(function () {
  
  angular.module('gamekeeper.game.levels', [
    'gamekeeper.game.levels.controllers',
    'gamekeeper.game.levels.directives',
    'gamekeeper.game.levels.factories'
  ]);
  
})();
class AddPictureToBadges < ActiveRecord::Migration
  def change
    add_column :badges, :picture, :string
  end
end

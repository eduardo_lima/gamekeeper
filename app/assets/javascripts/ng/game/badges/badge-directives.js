(function () {

  /**
   * Diretiva do panel de game badges
   *
   * @namespace game.directives
   * @class gameBadgesDirective
   * @constructor
   */
  var gameBadgesDirective = function () {
    return {
      templateUrl: "assets/views/games/directives/_badges-panel.html",
      scope: {
        game: "="
      },
      controller: 'GameBadgeCtrl'
    }  
  }
    
  /**
   * Módulo de diretivas de game
   * 
   * @module game.directives
   */
  angular.module('gamekeeper.game.badges.directives', [])
    .directive('gkGameBadges', gameBadgesDirective);

})();





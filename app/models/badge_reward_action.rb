class BadgeRewardAction < ActiveRecord::Base
  belongs_to :badge_reward_condition
  belongs_to :action

  validates :action_id,
            :presence   => { message:    I18n.t('activerecord.badge_reward_action.errors.messages.action.presence')}
  validates :times_to_achieve,
            :presence   => { :message => I18n.t('activerecord.badge_reward_action.errors.messages.times_to_achieve.presence') }


            # Colocar validação depois para não permitir cadastrar actions repetidas

  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) or inc.include?(:api) then
      hash[:verb] = self.action.verb
      hash[:times_to_achieve] = self.times_to_achieve
    end
    
    return hash
  end

  def remove_this
    self.removed = true
    self.save
  end

end

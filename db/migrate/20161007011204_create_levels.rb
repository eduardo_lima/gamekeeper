class CreateLevels < ActiveRecord::Migration
  def change
    create_table :levels do |t|
      t.string :name
      t.integer :required_points
      t.boolean :active
      t.attachment :image

      t.boolean :hidden, default: false
      t.boolean :removed, default: false

      t.references :game, index: true, foreign_key: true
    end
  end
end

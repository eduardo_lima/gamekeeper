class GatekeeperService

  def initialize
		# @root_url = "http://api.gatekeeper.dev:3001"
    @root_url = "https://ragekeeper.herokuapp.com/api/v1"
	end

  # Chama o serviço de registrar usuário da aplicação Gatekeeper.
  # Retorna a resposta da requisição em json.
  #
  # Necessário email, password e confirmação.
  def signup name, email, password, password_confirmation
    url = "#{@root_url}/users"
    params = { 
      user: {
        name: name,
        email: email, 
        password: password,
        password_confirmation: password_confirmation
      }
    }
    begin
      response = JSON.parse(RestClient.post(url, params))
      ServiceResponse.new(true, message: response["messages"], status: response["status"])
    rescue RestClient::UnprocessableEntity => e
      response = JSON.parse(e.response)
			ServiceResponse.new(false, message: response["messages"], status: response["status"])
    rescue => e
			ServiceResponse.new(false, status: :internal_server)
		end
  end

  # Chama o serviço de ativar conta do usuário da aplicação Gatekeeper.
  # Retorna a resposta da requisição em json.
  #
  # Necessário email e token 
  def activate email, activation_token
    url = "#{@root_url}/account_activations/#{activation_token}/activate"
    params = {
      email: email
    }
    begin
      response = JSON.parse(RestClient.post(url, params))
      ServiceResponse.new(true, message: response["messages"],
                                status: response["status"],
                                params: response["data"])
    rescue RestClient::Unauthorized => e
      response = JSON.parse(e.response)
			ServiceResponse.new(false, message: response["messages"], status: response["status"])
    rescue => e
			ServiceResponse.new(false, status: :internal_server)
		end
  end

  # Chama o serviço de autenticação do usuário na aplicação Gatekeeper
  # Retorna a resposta da requisição em json.
  #
  # Necessário email e senha
  def signin email, password
    url = "#{@root_url}/sessions.json"
    params = { 
      session: {
        email: email, 
        password: password
      }
    }
    begin
      response = JSON.parse(RestClient.post(url, params))
      ServiceResponse.new(true, message: response["messages"],
                                status: response["status"],
                                params: response["data"])
    rescue RestClient::Unauthorized => e
      response = JSON.parse(e.response)
      ServiceResponse.new(false, message: response["messages"], status: response["status"])
    rescue => e
			ServiceResponse.new(false, status: :internal_server)
		end
  end

  def current_user auth_token
    url = "#{@root_url}/sessions/get_current_user"
    params = {
      auth_token: auth_token
    }
    begin
      response = JSON.parse(RestClient.get(url, {:Authorization => auth_token}))
      ServiceResponse.new(true, message: response["messages"], 
                                status: response["status"],
                                params: response["data"])
    rescue RestClient::Unauthorized => e
      response = JSON.parse(e.response)
      ServiceResponse.new(false, message: response["messages"], status: response["status"])
    rescue => e
      ServiceResponse.new(false, status: :internal_server)
    end
  end

  def signout auth_token
    url = "#{@root_url}/sessions/#{auth_token}"
    begin
      response = JSON.parse(RestClient.delete(url))
      ServiceResponse.new(true, message: response["messages"], status: response["status"])
    rescue => e
      ServiceResponse.new(false, status: :internal_server)
    end
  end

  def send_recover_password_instructions email
    url = "#{@root_url}/password_recover"
    params = {
      email: email
    }
    begin
      response = JSON.parse(RestClient.post(url, params))
      ServiceResponse.new(true, message: response["messages"], status: response["status"])
    rescue RestClient::ResourceNotFound => e
      response = JSON.parse(e.response)
      ServiceResponse.new(false, message: response["messages"], status: response["status"])
    rescue => e
      ServiceResponse.new(false, status: :internal_server)
    end
  end

  def recover_password token, email, password, password_confirmation
    url = "#{@root_url}/password_recover/#{token}/recover"
    params = {
      user: {
        email: email,
        password: password,
        password_confirmation: password_confirmation
      }
    }
    begin
      response = JSON.parse(RestClient.post(url, params))
      ServiceResponse.new(true, message: response["messages"],
                                status: response["status"],
                                params: response["data"])

    rescue RestClient::UnprocessableEntity => e
      response = JSON.parse(e.response)
      ServiceResponse.new(false, message: response["messages"], status: response["status"])
    rescue => e
      ServiceResponse.new(false, status: :internal_server)
    end
  end

end
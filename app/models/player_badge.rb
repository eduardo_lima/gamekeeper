class PlayerBadge < ActiveRecord::Base
  belongs_to :player
  belongs_to :badge

   def to_hash(inc=:all)
     inc = Array(inc)
     hash = {} 

     if inc.include?(:all) or inc.include?(:api) then
       hash[:badge_photo] = self.badge.picture.url.present?? self.badge.picture.url : "game-badge-icon.png"
       hash[:badge_name] = self.badge.name
       hash[:earned_at] = self.created_at
     end

     return hash
   end
end

class ApplicationController < ActionController::Base
  include SessionsHelper
  include UsersHelper

  before_action :set_locale

  before_action :current_user # por enquanto vou deixar aqui

  respond_to :json

  def angular
  	render 'layouts/application'
  end

  # Método para carregar a sessão do usuário
  # Retorna o objeto User 
  #
  # Mantém a instância @current_user para usar nas actions
  def current_user
    reply_data = send_current_user_request.params
    @current_user = User.new(reply_data) if reply_data
  end

  # Método para obrigar o usuário se logar
  # Retorna a url de redirecionamento
  #
  # Por enquanto redireciona para a tela de login...
  def require_user
    unless logged_in?
      if Rails.env.development?
        redirect_to "http://gamekeeper.com.br:3000/#/login"
      else 
        redirect_to "https://furb-gamekeeper.herokuapp.com/login"
      end
    end
  end

  def set_locale
    # I18n.locale = params[:locale] || I18n.default_locale
    I18n.default_locale = :pt_BR
  end
end


class CreatePerformedActions < ActiveRecord::Migration
  def change
    create_table :performed_actions do |t|

      t.references :action, index: true, foreign_key: true
      t.references :player, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class Game < ActiveRecord::Base
  
  # um game eh composta por varias acoes
  has_many :actions, dependent: :destroy
  has_many :badges, dependent: :destroy
  has_many :players, dependent: :destroy
  has_many :levels, dependent: :destroy
  
  # um game tem uma imagem de referencia
  has_attached_file :image, :styles => { :small => "150x150>" },
                  :default_url => "/assets/game-icon.png",
                  :url  => "/assets/games/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/games/:id/:style/:basename.:extension"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  mount_uploader :picture, PictureUploader

  # o nome do game eh obrigatorio e aceita 50 caracteres
  validates :name,
            :presence => { :message => I18n.t('activerecord.game.errors.messages.name.presence') },
            :length   => { :maximum => 50, :message => I18n.t('activerecord.game.errors.messages.name.length') }
  # o objetivo do game deve ser descrito dentro de 250 caracteres
  validates :objective,
            :length   => { :maximum => 250, :message => I18n.t('activerecord.game.errors.messages.objective.length') }
  # o id do game nao deve conter espacos, obrigatorio e deve ser unico
  VALID_NAME_REGEX = /\A[a-z0-9\-_]+\z/
  validates :unique_name,
            :presence   => { :message => I18n.t('activerecord.game.errors.messages.unique_name.presence') },
            :length     => { :maximum => 50, :message => I18n.t('activerecord.game.errors.messages.unique_name.length') },
            :format     => { :with => VALID_NAME_REGEX, :message => I18n.t('activerecord.game.errors.messages.unique_name.format') },
            :uniqueness => { :message => I18n.t('activerecord.game.errors.messages.unique_name.uniqueness') }
  # o id do usuario que eh proprietario do game eh obrigatorio
  validates :user_id,
            :presence => { :message => I18n.t('activerecord.game.errors.messages.user_id.presence') }
  
  # listar por id de usuário
  scope :list, ->(id) { where(user_id: id, removed: false) }

  after_create :add_hidden_actions
  after_create :add_first_level
  after_create :generate_api_key

  # Converte os campos do model de acordo com a opcao determinada.
  # Retorna um hash com os campos do model.
  #
  # O parametro 'inc' define qual serao os campos incluidos no 
  # hash. Quando 'inc' for <tt>:all</tt> todos os campos devem 
  # estar no hash. Quando 'inc' for <tt>:basic</tt> os campos de id,
  # nome e imagem do game devem estar no hash. Quando 'inc' for 
  # <tt>:stats</tt> os campos que contabilizam as actions, badges, 
  # levels e players devem estar no hash.
  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) then
      hash[:id] = self.id
      hash[:user_id] = self.user_id
      hash[:name] = self.name
      hash[:objective] = self.objective
      hash[:unique_name] = self.unique_name
      hash[:image_url] = self.picture.url rescue "game-icon.png"  
      hash[:access_token] = APIKey.find_by_unique_name(self.unique_name).access_token
    end

    if inc.include?(:all) or inc.include?(:api) then
      hash[:id] = self.id
      hash[:owner_id] = self.user_id
      hash[:name] = self.name
      hash[:unique_name] = self.unique_name
    end
    
    if inc.include?(:all) or inc.include?(:stats) then
      hash[:number_of_actions] = self.number_of_actions
      hash[:number_of_badges] = self.number_of_badges
      hash[:number_of_levels] = self.number_of_levels
      hash[:number_of_players] = self.number_of_players
    end
    
    return hash
  end

  def generate_api_key
    APIKey.create!(unique_name: self.unique_name)
  end

  def remove_this
    self.removed = true
    self.save
  end

private 

  def add_hidden_actions
    file = File.open("#{Rails.root}/public/assets/game-action-icon.png")
    ao = ActionObject.new
    ao.article = "no"
    ao.singular = "jogo"
    ao.plural = "jogos"
    Action.create!(
      game_id: self.id,
      verb: "entrar",
      past_tense: "entrou",
      points: 20,
      image: file,
      hidden: true,
      action_object: ao
    )
    file.close
  end

  def add_first_level
    file = File.open("#{Rails.root}/public/assets/game-level-icon.png")
    Level.create!(
      game_id: self.id,
      name: "Sem nível",
      required_points: 0,
      active: true,
      hidden: true,
      image: file
    )
    file.close
  end

  # def prepare_territory
  #   create_schema
  #   load_tables
  # end

  # def create_schema
  #   PgAwesomeTools.create_schema self.unique_name unless PgAwesomeTools.schemas.include? self.unique_name
  # end

  # def load_tables
  #   # return if Rails.env.test?
  #   PgAwesomeTools.set_search_path self.unique_name, false
  #   load "#{Rails.root}/db/schema.rb"
  #   PgAwesomeTools.restore_default_search_path
  # end


end

1.times do |n|
  n += 1
  puts "Criando game_#{n}"
  
  begin
    game = Game.create!(
      user_id: 1,
      name: Faker::Pokemon.name,
      unique_name: Faker::Lorem.word,
      objective: Faker::Hipster.sentence(10, true), 
      number_of_players: 0,
      number_of_actions: 0,
      number_of_badges: 0,
      number_of_levels: 0
    )

    puts "Criando ações exemplo..."
    file = File.open("#{Rails.root}/public/assets/game-action-icon.png")
    ao = ActionObject.new
    ao.article = "uma"
    ao.singular = "publicação"
    ao.plural = "publicações"
    Action.create!(
      game_id: game.id,
      verb: "compartilhar",
      past_tense: "compartilhou",
      points: 60,
      image: file,
      action_object: ao
    )

    ao = ActionObject.new
    ao.article = "uma"
    ao.singular = "publicação"
    ao.plural = "publicações"
    Action.create!(
      game_id: game.id,
      verb: "curtir",
      past_tense: "curtiu",
      points: 20,
      image: file,
      action_object: ao
    )
    
    ao = ActionObject.new
    ao.article = "uma"
    ao.singular = "publicação"
    ao.plural = "publicações"
    Action.create!(
      game_id: game.id,
      verb: "responder",
      past_tense: "respondeu",
      points: 45,
      image: file,
      action_object: ao
    )
    file.close

    puts "Criando níveis exemplo..."
    file = File.open("#{Rails.root}/public/assets/game-level-icon.png")
    Level.create!(
      game_id: game.id,
      name: "Novato",
      required_points: 0,
      active: true,
      image: file
    )

    Level.create!(
      game_id: game.id,
      name: "Apprentice",
      required_points: 100,
      active: true,
      image: file
    )

    Level.create!(
      game_id: game.id,
      name: "Vetera",
      required_points: 1000,
      active: true,
      image: file
    )

    Level.create!(
      game_id: game.id,
      name: "Master",
      required_points: 8000,
      active: true,
      image: file
    )
    file.close

    puts "Criando jogadores fake..."
    file = File.open("#{Rails.root}/public/assets/game-player-icon.png")
    30.times do |n|
      Player.create!(
        game_id: game.id,
        name: Faker::Name.name_with_middle,
        points_obtained: 0,
        image: file
      )
    end

  rescue ActiveRecord::RecordInvalid => invalid
    puts invalid.record.errors.inspect
  end
  file.close
end 

class User < ActiveRecord::Base

  # metodos de acesso para token de lembre-se, ativacao e recuperacao de senha
  attr_accessor :remember_token, :activation_token, :reset_token
  # um usuario pode ter varios games
  has_many :games, dependent: :destroy

  # antes de salvar o email deve estar com letras minusculas
  before_save   :downcase_email
  # antes de criar o usuario deve ser criado um codigo de ativacao
  before_create :create_activation_digest
  
  # o nome do usuario eh obrigatorio e pode conter ate 50 caracteres
  validates :name,
            :presence   => { :message => "Name can't be blank." },
            :length     => { :maximum => 50, :message => "Name is too long." }
  # o email deve estar ter o formato correto, obrigatorio e unico
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email,
            :presence   => { :message => "Email can't be blank." },
            :length     => { :maximum => 255, :message => "Name is too long." },
            :format     => { :with => VALID_EMAIL_REGEX, :message => "Email is not valid." },
            :uniqueness => { :case_sensitive => false, :message => "Email already exists." }
  # a senha eh obrigatorio e deve conter no minimo 6 caracteres
  validates :password,
            :presence   => { :message => "Password can't be blank." },
            :length     => { :minimum => 6, :message => "Password is too small." }  #allow_nil: true ?
	has_secure_password


  # Metodo de classe para criar mensagem digest.
  # Retorna string encriptada.
  #
  # O parametro 'string' eh a mensagem que sera encriptada 
  # e gravada na base.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Metodo de classe para criar codigo token que sera
  # usado para identificar a ativacao ou recuperacao de senha
  # Retorna um novo codigo token base 64
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Metodo para verificar gravar a mensagem para autencicar
  # automaticamente
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Metodo para verificar o campo encriptado eh o mesmo
  # que o do token.
  # Retorna true se o token tem o mesmo valor da base
  # 
  # O parametro 'attribute' eh o campo que sera usado
  # para fazer a verificacao, os valores serao: 
  # <tt>:activation</tt>, <tt>reset</tt>
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Metodo para esquecer o usuario, grava vazio na base
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Metodo para criar codigo de recupercao de senha e 
  # data de pedido de recuperacao
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Metodo para enviar email de recuperacao de senha
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Metodo para verificar a expiracao de codigo de 
  # recuperacao de senha
  # Retorna true se passou de duas horas
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  # Converte os campos do model de acordo com a opcao determinada.
  # Retorna um hash com os campos do model.
  #
  # O parametro 'inc' define qual serao os campos incluidos no 
  # hash. Quando 'inc' for <tt>:all</tt> todos os campos devem 
  # estar no hash. Quando 'inc' for <tt>:basic</tt> os campos de id,
  # email e nome devem estar no hash.
  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) then
      hash[:id] = self.id
      hash[:email] = self.email
      hash[:name] = self.name
    end
    
    return hash
  end

private

  # Metodo para transformar email em letras minusculas
  def downcase_email
    self.email = email.downcase
  end

  # Metodo para criar codigo de ativacao de conta
  def create_activation_digest
    self.activation_token  = User.new_token
    self.activation_digest = User.digest(activation_token)
  end

end

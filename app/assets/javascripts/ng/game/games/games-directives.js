(function () {
  
  /**
   * Diretiva do panel de game settings
   *
   * @namespace game.directives
   * @class gameSettingsDirective
   * @constructor
   */
  var gameSettingsDirective = function () {
    return {
      templateUrl: "assets/views/games/directives/_settings-panel.html",
      scope: {
        game: "="
      },
      controller: 'GameSettingsCtrl'
    }
  }
  
    
  /**
   * Módulo de diretivas de game
   * 
   * @module game.directives
   */
  angular.module('gamekeeper.game.directives', [])
    .directive('gkGameSettings', gameSettingsDirective);

})();





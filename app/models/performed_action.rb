class PerformedAction < ActiveRecord::Base
  belongs_to :action
  belongs_to :player
  has_many :performed_action_properties, dependent: :destroy

  accepts_nested_attributes_for :performed_action_properties

  after_create :update_player

  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}

    if inc.include?(:all) or inc.include?(:seven_days_stats) then
      hash[:verb] = self.action.verb
      hash[:days] = {}
      hash[:days] = by_date_block do |date, limit|
       
        total_of_all_actions = PerformedAction.joins(:action).where('actions.game_id = ? AND DATE(created_at) = ?', self.action.game_id, date).count
        total_of_this_action = PerformedAction.joins(:action).where('game_id = ? AND action_id = ? AND DATE(created_at) = ?', 
          self.action.game_id, self.action.id, date).count

        perc = 0
        perc = ((total_of_this_action.to_f / total_of_all_actions.to_f) * 100).round(2) if total_of_all_actions > 0
    
        {
          date: date.strftime("%m-%d-%Y"),
          times_performed: total_of_this_action,
          times_performed_perc: "#{perc}%" 
        }
      end
    end

    hash[:days].sort! { |a,b| b[:date] <=> a[:date]}

    return hash
  end

private

  def by_date_block
    days_passing = 7.days.ago.to_date
    today = Date.today

    results = []

    begin
      results << yield(days_passing, today)
      days_passing += 1
    end while days_passing < today 
    # pra hoje
    results << yield(days_passing, today)

    return results
  end

#-----------------------------------------------------------------------------------------
  def update_player

    # adicionar pontuação do jogador
    puts ">>>>> Ganhou #{self.action.points} de ação..."
    self.player.points_obtained += self.action.points

    # verificar se ganhou prêmios + pontuação bônus

    # pegar insignias do game e da ação performada
    # aqui vai entrar se tiver registro, então tá de boa
    Badge.joins(badge_reward_condition: :badge_reward_actions).where(game_id: self.player.game_id, badge_reward_actions: { action_id: self.action.id }).each do |b|
      # verificar se passa na regra entre definição e ações já performadas
      puts ">>>>> #{b.name}"
      puts ">>>>> #{self.action.verb}"

      # definir o tipo de regra pra ganhar insígnias
      condition_type = b.badge_reward_condition.condition_type
      # pegar a última vez que foi ganho a insígnia (se já foi ganho)
      last_achieved_on = PlayerBadge.where(badge: b).order('created_at DESC').first

      # verificar o tipo de condição
      case condition_type
      when "any"
        # verificar se a ação corrente atingiu a quantidade de vezes definidas (não considerar ações repetidas, pega a primeira mesmo)
        # condição de reject! para ignorar se o usuário inclui ações iguais dentro da condição
        times_to_achieve = b.badge_reward_condition.badge_reward_actions.where(action_id: self.action.id).first.times_to_achieve

        times_performed = 0
        # contabilizar quantas vezes a ação foi executada depois do prêmio anterior
        if last_achieved_on.present? then
          times_performed = PerformedAction.where('player_id = ? AND action_id = ? AND created_at > ?', 
            self.player.id, self.action.id, last_achieved_on.created_at).count
        else
          puts ">>> Não ganhou nada ainda"
          # quando não ganhou nada ainda
          times_performed = PerformedAction.where('player_id = ? AND action_id = ?', self.player.id, self.action.id).count
        end

        puts "A performar #{times_to_achieve}"
        puts "Performou #{times_performed}"

        # daí precisa verificar se a condição passou entre times_to_achieve e times_performed, gerando PlayerBadge
        if times_performed >= times_to_achieve then
          PlayerBadge.create!(
            player_id: self.player.id,
            badge_id: b.id
          )
          # ponts de bonus
          puts ">>>>> Ganhou #{b.bonus_points} de insígnias..."
          self.player.points_obtained += b.bonus_points
        end

      when "all"

        earned_badge = false

        b.badge_reward_condition.badge_reward_actions.each do |bra|

          # irá ter que validar cada ação pra ver se ganha a insíginia
          # a lógica aqui será negar a recompensa caso n seja cumprido a condição
          times_to_achieve = bra.times_to_achieve

          # contabilizar as vezes que a ação foi executada
          if last_achieved_on.present? then
            times_performed = PerformedAction.where('player_id = ? AND action_id = ? AND created_at > ?', 
              self.player.id, bra.action.id, last_achieved_on.created_at).count
          else
            puts ">>> Não ganhou nada ainda"
            # quando não ganhou nada ainda
            times_performed = PerformedAction.where('player_id = ? AND action_id = ?', self.player.id, bra.action.id).count
          end

          # verifica se ainda ganha o prêmio ou não
          earned_badge = times_performed >= times_to_achieve

          break if earned_badge == false # já sai se não cumpriu todas as actions
        end

        if earned_badge then
          PlayerBadge.create!(
            player_id: self.player.id,
            badge_id: b.id
          )
          # ponts de bonus
          puts ">>>>> Ganhou #{b.bonus_points} de insígnias..."
          self.player.points_obtained += b.bonus_points
        end
      end
    end

    # verificar se alterou level_id
    new_level = Level.where('game_id = ? AND required_points <= ? AND active = true', self.player.game_id, self.player.points_obtained).order('required_points DESC').first

    logger.debug "New level é #{new_level.inspect}"

    if new_level.present? then
      logger.debug "não é pra entrar aqui"
      self.player.level_id = new_level.id
    end

    # enfim, salvar
    self.player.save
  end

end

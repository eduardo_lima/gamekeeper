(function () {

  var gameLevelsDirective = function () {
    return {
      templateUrl: "assets/views/games/directives/_levels-panel.html",
      scope: {
        game: "="
      },
      controller: 'GameLevelCtrl'
    }  
  }
    
  /**
   * Módulo de diretivas de game
   * 
   * @module game.directives
   */
  angular.module('gamekeeper.game.levels.directives', [])
    .directive('gkGameLevels', gameLevelsDirective);

})();





module AccountActivationsHelper

  def request_activation email, token
    GatekeeperService.new.activate(email, token)
  end

end

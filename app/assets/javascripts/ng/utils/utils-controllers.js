
(function () {
  
  /**
   * Controller do Navbar
   *
   * @namespace utils.controllers
   * @class NavbarController
   * @constructor
   */
  var NavbarController = ['$scope', 'sessionFactory', '$state', function($scope, sessionFactory, $state){
    
    if (!$scope.user) {
      sessionFactory.loggedIn()
        .then(function (response) { 
          $scope.user = response.data.user; 
        })
        .catch(function (response) { 
          if (response.data.message) {
            $state.go("app.user.login");
          }
        })
    }
    
    $scope.logout = function () {
      sessionFactory.logout().then(function (response) {
        $state.go("app.user.login");
      })
    }
  }];

  /**
   * Módulo de controllers dos utils
   *
   * @module utils
   */  
  angular.module('gamekeeper.utils.controllers', [])
    .controller('NavbarController', NavbarController);
})();

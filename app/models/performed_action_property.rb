class PerformedActionProperty < ActiveRecord::Base
  belongs_to :object_property
  belongs_to :performed_action

  validates :object_property_id,
            :presence   => { :message => I18n.t('activerecord.performed_action_property.errors.messages.object_property_id.presence') }
  validates :performed_action_id,
            :presence   => { :message => I18n.t('activerecord.performed_action_property.errors.messages.performed_action_id.presence') }
  validates :value,
            :presence   => { :message => I18n.t('activerecord.performed_action_property.errors.messages.value.presence') }

  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}

    if inc.include?(:all) or inc.include?(:basic) then
     hash[:name] = self.object_property.name
     hash[:value] = self.value
    end
    
    return hash
  end
end

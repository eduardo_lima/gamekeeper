class CreateBadgeRewardConditions < ActiveRecord::Migration
  def change
    create_table :badge_reward_conditions do |t|
      t.string :condition_type

      t.references :badge, index: true, foreign_key: true
    end
  end
end

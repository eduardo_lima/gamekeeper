class SessionsController < ApplicationController

  def create 
    user_email = params[:session][:email].downcase
    user_password = params[:session][:password]
    remember_me = params[:session][:remember_me]

    # chama endpoint pra logar usuários
    reply = send_signin_request(user_email, user_password)
    # salvar o token de acesso para já usar a plataforma, se tudo der certo
    save_session reply.params['auth_token'] if reply.success
    # lembrar sessão
    remember_session reply.params['auth_token'] if reply.success && remember_me

    # responde
    render status: reply.status, json: { message: reply.message, user: reply.params }
  end

  # Método para verificar se há sessão ativa do usuário
  # Retorna as informações básicas do usuário
  def logged_in
    reply = send_current_user_request

    puts reply.inspect
    render status: reply.status, json: { message: reply.message, user: reply.params }
  end

  # Método para destruir a sessão de usuário
  # Retorna mensagem de sessão encerrada com sucesso
  def destroy
    request_sign_out
    
    respond_with do |format|
      format.json { render json: { message: "User has been logged out!" }, status: :ok}
    end
  end



end

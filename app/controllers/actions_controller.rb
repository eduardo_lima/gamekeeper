include ApplicationHelper

class ActionsController < ApplicationController
  # antes de qualquer ação, o usuário deverá estar logado
  before_action :require_user
  # antes de criar ou atualizar o game action é preciso processar o parâmetro file
  before_filter :process_params, only: [:create, :update]
  # antes de criar, listar ou deletar é preciso verificar se o game pertence ao usuário
  before_action :correct_user, only: [:create, :update, :destroy, :index]
  before_filter :current_action, only: [:update, :destroy]
  before_filter :conjure_action_instance, only: [:create, :update]

  # Traz todos as game actions
  def index
    respond_with Action.where(game_id: params[:game_id], hidden: false, removed: false).collect { |a| a.to_hash(:basic) }
  end

  # Cria uma nova game action
  def create
    if @action.save
      respond_with do |format|
        format.json { render json: { :flash => "Game action #{@action.verb} has been created.", :id => @action.id }, status: :ok }
      end
    else
      respond_with @action
    end
  end

  # Atualiza a game action
  def update
    if @action.save
      respond_with do |format|
        format.json { render json: { :flash => "Game action #{@action.verb} has been updated.", :id => @action.id }, status: :ok }
      end
    else
      respond_with @action
    end
  end

  # Remove a game action
  def destroy
    if @action.remove_this
      respond_with do |format|
        format.json { render json: { :flash => "Game action #{@action.verb} has been destroyed.", :id => @action.id },status: :ok }
      end
    else
      respond_with do |format|
        format.json { render json: { :flash => "Something went wrong when destroying game action."}, status: :internal_server_error }
      end
    end
  end

private

  # Carrega a game action que existe
  def current_action
    @action = Action.find(params[:id])
  end

  # Define o que pode ser permitido inserir na action
  def permitted_params
    params.require(:game_action)
      .permit(:game_id, :verb, :past_tense, :points, :bonus_points, :image, :picture,
              :action_object_attributes => [:article, :singular, :plural], 
              :object_properties_attributes => [:name, :kind])
  end

  # Processa a imagem anexada
  def process_params
    params[:game_action] = JSON.parse(params[:game_action]).with_indifferent_access if hash.is_a?(String)

    if params[:file]
      params[:game_action][:image] = params[:file]
      params[:game_action][:picture] = params[:file]
    end
  end

  # Verificar se o usuário é autorizado a fazer isso
  def correct_user
    # Validar se o é mesmo dele
    unless current_user_is_owner?(params[:game_id])
      respond_with do |format|
        format.json { render json: { :flash => "This game doesn't belongs to you." }, status: :unauthorized }
      end
    end
  end

  # Preencher todos os campos do model action
  def conjure_action_instance
    # Verifica parametros permitidos
    params = permitted_params
    # Caso nao foi criado
    unless @action.present? then
      @action = Action.new
      @action.game_id = params[:game_id]
    end

    # Preenche os campos do modelo
    @action.verb = params[:verb]
    @action.past_tense = params[:past_tense]
    @action.points = params[:points]
    @action.image = params[:image]
    @action.picture = params[:image]
    @action.action_object = ActionObject.new unless @action.action_object.present?
    @action.action_object.article = params[:action_object_attributes][:article]
    @action.action_object.singular = params[:action_object_attributes][:singular]
    @action.action_object.plural = params[:action_object_attributes][:plural]
    
    @action.action_object.object_properties.map(&:remove_this)
    params[:object_properties_attributes].each do |key, property|
      p = ObjectProperty.new
      p.name = property[:name]
      p.kind = property[:kind]
      @action.action_object.object_properties << p
    end if params[:object_properties_attributes].present?
  end

end

(function () {

  var gamePlayersDirective = function () {
    return {
      templateUrl: "assets/views/games/directives/_players-panel.html",
      scope: {
        game: "=",
        admin: "="
      },
      controller: 'GamePlayerCtrl'
    }  
  }

  var gamePlayersDetailDirective = function () {
    return {
      templateUrl: "assets/views/games/directives/_players-panel-detail.html",
      scope: {
        game: "=",
        player: "="
      },
      controller: 'GamePlayerDetailCtrl'
    }  
  }

  var gamePlayersSandboxDirective = function () {
    return {
      templateUrl: "assets/views/games/directives/_players-panel-sandbox.html",
      scope: {
        game: "=",
        player: "="
      },
      controller: 'GamePlayerSandboxCtrl'
    }  
  }
    
  /**
   * Módulo de diretivas de game
   * 
   * @module game.directives
   */
  angular.module('gamekeeper.game.players.directives', [])
    .directive('gkGamePlayers', gamePlayersDirective)
    .directive('gkGamePlayersDetail', gamePlayersDetailDirective)
    .directive('gkGamePlayersSandbox', gamePlayersSandboxDirective);
})();





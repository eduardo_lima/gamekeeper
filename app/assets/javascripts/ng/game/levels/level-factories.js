(function () {

  var gameLevelFactory = ['$http', 'Upload', '$q', function ($http, Upload, $q) {
    
    var canceler;
    
    var _startFactory = function () {
      canceler = $q.defer(); 
    }
    
    var _stopFactory = function () {
      canceler.resolve('cancelled');
    }

    var _internalSave = function (gameLevel, method, url) {
      var image = gameLevel.file ? gameLevel.file: [];
      var options = {
        url: url,
        method: method,
        file: image,
        timeout: canceler.promise,
        file_form_data_name: gameLevel.name ? gameLevel.name: "",
        fields: {
          // traduzir para backend
          game_level: {
            game_id: gameLevel.game_id,
            name: gameLevel.name,
            required_points: gameLevel.required_points,
            active: gameLevel.active
          }
        }
      }
      return Upload.upload(options);  
    }
    
    var _save = function (gameLevel) {
      return _internalSave(gameLevel, 'POST', '/games/' + gameLevel.game_id + '/levels.json');
    }
    
    var _update = function (gameLevel) {
      return _internalSave(gameLevel, 'PUT', '/games/' + gameLevel.game_id + '/levels/' + gameLevel.id + '.json');
    }
    
    var _delete = function (gameLevel) {
      return $http.delete('/games/' + gameLevel.game_id + '/levels/' + gameLevel.id + '.json', { timeout: canceler.promise });
    }
    
    var _list = function (gameId) {
      return $http.get('/games/' + gameId + '/levels.json', { timeout: canceler.promise });
    }
    
    return {
      start: _startFactory,
      stop: _stopFactory,
      save: _save,
      update: _update,
      delete: _delete,
      list: _list
    }
  }];
  
  angular.module('gamekeeper.game.levels.factories', ['ngFileUpload'])
    .factory('gameLevelFactory', gameLevelFactory);

})();

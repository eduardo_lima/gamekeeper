(function () {

  /**
   * Diretiva do panel de game actions
   *
   * @namespace game.directives
   * @class gameActionsDirective
   * @constructor
   */
  var gameActionsDirective = function () {
    return {
      templateUrl: "assets/views/games/directives/_action-panel.html",
      scope: {
        game: "="
      },
      controller: 'GameActionCtrl'
    } 
  } 
    
  /**
   * Módulo de diretivas de game
   * 
   * @module game.directives
   */
  angular.module('gamekeeper.game.actions.directives', [])
    .directive('gkGameActions', gameActionsDirective);

})();





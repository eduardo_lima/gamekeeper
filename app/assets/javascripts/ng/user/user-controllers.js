
(function () {
  
  /**
   * Controller a tela de cadastro de usuario
   *
   * @namespace user.controllers
   * @class SignupCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $state Helper para controlar states
   * @param accountFactory Factory de chamadas de backend
   * @param labels Labels para os componentes da página
   * @param toastr Mensagens de feedback toast
   */
  var SignupCtrl = ['$scope', '$state', 'accountFactory', 'labels', 'toastr',
    function ($scope, $state, accountFactory, labels, toastr) {
    // lista de erros de front-end
    $scope.labels = labels;

    // método ao submitar o formulário, método sem parâmetro pois pega do escopo
    $scope.signup = function (user) {

      $scope.isSigningUp = true

      accountFactory.signup({
        name: user.name,
        email: user.email,
        password: user.password,
        password_confirmation: user.passwordConfirm
      }).then(function (response) {

        $scope.user = {};

        toastr.success($scope.labels.messages.toast.userWasSignedUp.text, $scope.labels.messages.toast.userWasSignedUp.title);

        $state.go('app.user.signupConfirmation');

      }).catch(function (response) {

        // isso é um unprocessable entity
        if (response.status == 422) {
          var attribute = Object.keys(response.data.message)[0];
          var values = response.data.message[attribute]; // vem em array cada conjunto de erros do rails (ou angular, sei lá)
          values.forEach(function (e, i, a) {
            toastr.error(e, $scope.labels.messages.toast.userWasNotSignedUp.title);
          });
        } else {
          toastr.error($scope.labels.messages.toast.userWasNotSignedUp.text, $scope.labels.messages.toast.userWasNotSignedUp.title);
        }

        $scope.isSigningUp = false;
      })
    }
  }];
  
  /**
   * Controller da tela de ativacao de cadastro
   *
   * @namespace user.controllers
   * @class ActivationCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $state Helper para controlar states
   * @param $stateParams Servico com os parametros do state
   * @param accountFactory Factory de chamadas de backend
   */
  var ActivationCtrl =  ['$scope', '$state', '$stateParams', 'accountFactory',
    function ($scope, $state, $stateParams, accountFactory) {

    $scope.isActivatingAccount = true;

    accountFactory.activate($stateParams.token, $stateParams.email).then(function (response) {
      $scope.redirectTo = "#/games";
      $scope.message = "Now you are ready to gamify!";
      $scope.iconClass = "fa-arrow-circle-right";
      $scope.isActivatingAccount = false;
    }).catch(function (response) {
      $scope.redirectTo = "#/signup";
      $scope.message = "Something wrong happened"
      $scope.iconClass = "fa-undo";
      $scope.isActivatingAccount = false;
    });
  }];


  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------

  /**
   * Controller a tela de envio de email para reset de senha
   *
   * @namespace user.controllers
   * @class PasswordRecoverSenderCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $state Helper para controlar states
   * @param accountFactory Factory de chamadas de backend
   * @param labels Labels para os componentes da página
   * @param toastr Mensagens de feedback toast
   */
  var PasswordRecoverSenderCtrl =  ['$scope', '$state', 'accountFactory', 'labels', 'toastr',
    function ($scope, $state, accountFactory, labels, toastr) {
    // lista de erros de front-end
    $scope.labels = labels;

    // tentará enviar a email de reset
    $scope.sendRequest = function (email) {

      $scope.isSendingEmail = true;

      accountFactory.sendRecoverEmail(email).then(function (response) {

        $scope.email = "";
        $scope.isSendingEmail = false;

        toastr.success(response.data.message, $scope.labels.messages.toast.emailWasSent.title);

        $state.go('app.user.passwordRecoverEmail');

      }).catch(function (response) {

        $scope.isSendingEmail = false;

        toastr.error(response.data.message, $scope.labels.messages.toast.emailWasNotSent.title);

      })
    }
  }];

  /**
   * Controller da tela para trocar a senha usando o token enviado no email
   *
   * @namespace user.controllers
   * @class PasswordRecoverFormCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $state Helper para controlar states
   * @param $stateParams Servico com os parametros do state
   * @param accountFactory Factory de chamadas de backend
   * @param labels Labels para os componentes da página
   * @param toastr Mensagens de feedback toast
   */
  var PasswordRecoverFormCtrl = ['$scope', '$state', '$stateParams', 'accountFactory', 'labels', 'toastr',
    function ($scope, $state, $stateParams, accountFactory, labels, toastr) {
    // lista de erros de front-end
    $scope.labels = labels;

    $scope.user = {};
    $scope.user.email = $stateParams.email;

    $scope.isResetting = false;

    // tentará enviar a email de reset
    $scope.reset = function (user) {

      $scope.isResetting = true;

      accountFactory.recoverPassword($stateParams.token, user).then(function (response) {

        $scope.isResetting = false;

        toastr.success(response.data.message, $scope.labels.messages.toast.passwordWasRecovered.title);

        $state.go('app.game.list');

      }).catch(function (response) {

        $scope.isResetting = false;

        // isso é um unprocessable entity
        if (response.status == 422) {
          if (response.data.errors) {
            var attribute = Object.keys(response.data.errors)[0];
            var values = response.data.errors[attribute]; // vem em array cada conjunto de erros do rails (ou angular, sei lá)
            values.forEach(function (e, i, a) {
              toastr.error(e, $scope.labels.messages.toast.passwordWasNotRecovered.title);
            });
          } else if (response.data.message) {
            toastr.error(response.data.message, $scope.labels.messages.toast.passwordWasNotRecovered.title);
          }
        } else {
          toastr.error($scope.labels.messages.toast.passwordWasNotRecovered.text, $scope.labels.messages.toast.passwordWasNotRecovered.title);
        }

      })
    }
  }];
  

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  
  
  /**
   * Controller da tela para editar a conta do usuario
   *
   * @namespace user.controllers
   * @class EditAccountCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $state Helper para controlar states
   * @param session Informacoes da sessao do usuario
   * @param accountFactory Factory de chamadas de backend
   * @param labels Labels para os componentes da página
   * @param toastr Mensagens de feedback toast
   */
  var EditAccountCtrl = ['$scope', '$state', 'labels', 'miscLabels', 'accountFactory', 'toastr',
    function ($scope, $state, labels, miscLabels, accountFactory, toastr) {
    // obtem todas as labels para renderizar no html
    $scope.labels = labels;
    $scope.miscLabels = miscLabels;

    $scope.isUpdating = false;

    $scope.save = function (user) {

      $scope.isUpdating = true;

      accountFactory.update(user).then(function (response) {

        toastr.success($scope.labels.messages.toast.accountWasUpdated.text, $scope.labels.messages.toast.accountWasUpdated.title);

        $scope.isUpdating = false;

      }).catch(function (response) {

        // isso é um unprocessable entity
        if (response.status == 422) {
          var attribute = Object.keys(response.data.errors)[0];
          var values = response.data.errors[attribute]; // vem em array cada conjunto de erros do rails (ou angular, sei lá)
          values.forEach(function (e, i, a) {
            toastr.error(e, $scope.labels.messages.toast.accountWasNotUpdated.title);
          });
        } else {
          toastr.error($scope.labels.messages.toast.accountWasNotUpdated.text, $scope.labels.messages.toast.accountWasNotUpdated.title);
        }

        $scope.isUpdating = false;

      })
    }
  }];


  /**
   * Módulo de controllers de users
   * 
   * @module user.controllers
   */
  angular.module('gamekeeper.user.controllers', [])
    .controller('SignupCtrl', SignupCtrl)
    .controller('ActivationCtrl', ActivationCtrl)
    .controller('PasswordRecoverSenderCtrl', PasswordRecoverSenderCtrl)
    .controller('PasswordRecoverFormCtrl', PasswordRecoverFormCtrl)
    .controller('EditAccountCtrl', EditAccountCtrl);

})();

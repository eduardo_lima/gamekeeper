(function () {

  /**
   * Classe que possui métodos com as labels da aplicação
   * @memberof gamekeeper.labels
   * @ngdoc factory
   * @name labelsFactory
   */
  var labelsFactory = function() {
    
    /**
     * Labels gerais usadas na aplicação
     *
     * @method _shared
     * @return {Object} Objeto com as labels
     */
    var _shared = function () {
      return {
        home: 'Home',
        game: 'Jogo',
        menu: {
          back: 'Voltar',
          overview: 'Resumo',
          players: 'Jogadores',
          actions: 'Ações',
          badges: 'Insígnias',
          levels: 'Níveis',
          settings: 'Configurações',
          collapse: 'Expandir'
        }
      }
    };
    
    /**
     * Labels na tela de cadastro de usuario
     *
     * @method _signup
     * @return {Object} Objeto com as labels
     */
    var _signup = function () {
      return {
        heading: "Inscreva-se",
        fields: {
          name: "Nome completo",
          email: "Email",
          password: "Senha",
          passwordConfirm: "Repetir senha"
        },
        buttons: {
          signup: "Enviar",
          login: "Entrar"
        },
        messages: {
          toast: {
            userWasSignedUp: {
              title: "Enviamos um email de confirmação ;)",
              text: "Verifique as instruções para saber mais"
            },
            userWasNotSignedUp: {
              title: "Falha para se inscrever...",
              text: "Alguma coisa deu errado"
            }
          }
        }
      };
    }
    
    /**
     * Labels na ativacao da conta do usuario
     *
     * @method _signup
     * @return {Object} Objeto com as labels
     */
    var _activation = function () {
      return {
        ready: "Tudo pronto, hora de gamificar!"
      }
    }
    
    /**
     * Labels na tela de enviar email para recuperacao de senha
     *
     * @method _passwordRecoverSender
     * @return {Object} Objeto com as labels
     */
    var _passwordRecoverSender = function () {
      return {
        heading: "Esqueceu a senha",
        fields: {
          email: "Email",
        },
        buttons: {
          send: "Enviar",
          back: "Voltar?"
        },
        messages: {
          toast: {
            emailWasSent: {
              title: "Enviamos um email de recuperação ;o",
              text: "Verifique as instruções para saber mais"
            },
            emailWasNotSent: {
              title: "Falha para enviar email",
              text: "Alguma coisa deu errado"
            }
          }
        }
      };
    }
    
    /**
     * Labels na tela de formulario para recuperacao de senha
     *
     * @method _passwordRecoverForm
     * @return {Object} Objeto com as labels
     */
    var _passwordRecoverForm = function () {
      return {
        heading: "Recuperar senha",
        fields: {
          password: "Senha",
          passwordConfirm: "Confirmar senha"
        },
        buttons: {
          update: "Atualizar",
        },
        messages: {
          toast: {
            passwordWasRecovered: {
              title: "Sua senha foi atualizada",
              text: "Agora você tem uma novinha em follha o/"
            },
            passwordWasNotRecovered: {
              title: "Falha para recuperar senha",
              text: "..."
            }
          }
        }
      };
    }
    
    /**
     * Labels usadas na edicao do usuario
     *
     * @method _editAccount
     * @return {Object} Objeto com as labels
     */
    var _editAccount = function () {
      return {
        heading: "Minha conta",
        headingSubtitle: "Configurações",
        fields: {
          name: "Nome",
          email: "Email",
          password: "Senha",
          confirmation: "Confirmar senha"
        },
        buttons: {
          save: "Atualizar"
        },
        messages: {
          toast: {
            accountWasUpdated: {
              title: "Seus dados foram atualizados",
              text: "Agora sim!"
            },
            accountWasNotUpdated: {
              title: "Falha para atualizar os dados",
              text: "Alguma coisa deu errado"
            }
          }
        },
        callouts: {
          account: {
            title: "Minha conta",
            description: "Sinta-se a vontade para mudar seu apelido, email ou senha ;)"
          }

        }
      }; 
    }
    
    /**
     * Labels usadas em game list
     *
     * @method _gameList
     * @return {Object} Objeto com as labels
     */
    var _gameList = function () {
      return {
        heading: "Lista",
        callouts: {
          list: {
            title: "Lista de jogos",
            description: "Crie ou melhore seus jogos, vamos lá!" 
          }
        },
        buttons: {
          add: "Novo"
        }
      }; 
    }
    
    /**
     * Labels usadas em game overview
     *
     * @method _gameOverview
     * @return {Object} Objeto com as labels
     */
    var _gameOverview = function () {
      return {
        heading: "Resumo",
        table: {
          actions: {
            columns: {
              action: "Ações",
              today: "Hoje",
              yesterday: "Ontem",
              twoDaysAgo: "2 dias atrás",
              threeDaysAgo: "3 dias atrás",
              fourDaysAgo: "4 dias atrás",
              fiveDaysAgo: "5 dias atrás",
              sixDaysAgo: "6 dias atrás",
              sevenDaysAgo: "7 dias atrás"
            }
          }
        },
        callouts: {
          actions: {
            title: "Relatório de ações",
            description: "Acompanhe o balanço de ações desempenhadas pelos jogadores durante a semana o/"
          },
          actionIsMissing: {
            title: "Aviso",
            description: "É necessário cadastrar ações para exibir esse relatório."
          },
          badges: {
            title: "Relatório de insígnias",
            description: "Veja a quantidade de jogadores que adquiriram cada insígnia."
          },
          levels: {
            title: "Relatório de níveis",
            description: "Veja a quantidade de jogadores que conseguiram alcançar os níveis do jogo."
          }
        },
        tabs: {
          tab1: "Ações",
          tab2: "Insígnias",
          tab3: "Níveis"
        }
      }
    };
    
    /**
     * Labels usadas em game overview
     *
     * @method _gameOverview
     * @return {Object} Objeto com as labels
     */
    var _gamePlayers = function () {
      return {
        heading: 'Jogadores',
        callouts: {
          basics: {
            title: 'Lista de jogadores',
            description: 'Veja de forma resumida as informações dos jogadores.'
          }
        },
        buttons: {
          show: "Mostrar"
        },
        fields: {
          name: "Imagem",
          level: "Nível",
          badges_obtained: "Insígnias",
          points_obtained: "Pontos"
        },
        messages: {
          toast: {
            players: {
              playersWereNotLoaded: {
                title: "Os jogadores não foram carregados.",
                text: "Tente novamente outra hora ;o"
              }
            }
          }
        }
      }  
    };

    var _gamePlayersDetail = function () {
      return {
        heading: 'Jogador',
        tabs: {
          tab1: "Eventos do jogador"
        },
        buttons: {
          show: "Mostrar"
        },
        fields: {
          name: "Imagem",
          level: "Nível",
          badges_obtained: "Insígnias",
          points_obtained: "Pontos"
        },
        messages: {
          toast: {
            player: {
              playerWereNotLoaded: {
                title: "O jogador não foi carregado.",
                text: "Tente novamente outra hora ;o"
              },
              playerInfoWasNotLoaded: {
                title: "As atividades do jogador não foram carregadas.",
                text: "Tente novamente outra hora -.-"
              }
            }
          }
        }
      }  
    };

    var _gamePlayersSandbox = function () {
      return {
        heading: 'Sandbox',
        messages: {
          toast: {
            sandbox: {
              sandboxWasNotLoaded: {
                title: "Sandbox não disponível.",
                text: "Bahh"
              },
              actionPerformed: {
                title: "A ação foi realizada.",
                text: "Tudo certo"
              },
              actionNotPerformed: {
                title: "A ação não foi realizada.",
                text: "Nem falo nada"
              }
            }
          }
        }
      }  
    };
    
    /**
     * Labels usadas em game actions
     *
     * @method _gameActions
     * @return {Object} Objeto com as labels
     */
    var _gameActions = function () {
      return {
        heading: 'Ações',
        callouts: {
          basics: {
            title: 'Informações básicas',
            description: 'Configure a ação que os jogadores podem fazer e pontuação ;)'
          },
          object: {
            title: 'Objeto da ação',
            description: 'Configure o objeto envolvido na ação.'
          },
          properties: {
            title: 'Propriedades do objeto',
            description: 'Configure as informações extras do objeto.'
          }
        },
        buttons: {
          save: "Salvar",
          upload: "Carregar",
          delete: "Remover"
        },
        fields: {
          file: "Imagem",
          identifier: "Verbo",
          pastTense: "Verbo no passado",
          points: "Pontuação",
          bonusPoints: "Pontuação bonus"
        },
        object: {
          title: "Objeto",
          article: "Que é "
        },
        properties: {
          title: "Propriedades",
          name: "Nome",
          kind: "Tipo"
        },
        messages: {
          identifier: {
            required: "Você precisa informar o verbo (único)"
          },
          pastTense: {
            required: "Você precisa infomar o verbo no passado"
          },
          points: {
            required: "Você precisa informar a pontuação",
          },
          toast: {
            actions: {
              actionWasSaved: {
                title: "A ação foi salva",
                text: "É isso aí ;)"
              },
              actionWasNotSaved: {
                title: "Falha para salvar a ação",
                text: "Algo deu errado =/"
              },
              actionWasDeleted: {
                title: "A ação foi removida",
                text: "Tudo bem ;'("
              },
              actionWasNotDeleted: {
                title: "Falha ao remover ação",
                text: "Algo deu errado =/"
              }
            }
          }
        }
      }      
    };
    
    /**
     * Labels usadas em game badges
     *
     * @method _gameBadges
     * @return {Object} Objeto com as labels
     */
    var _gameBadges = function () {
      return {
        heading: 'Insígnias',
        callouts: {
          basics: {
            title: 'Informações básicas',
            description: 'Configure a insígnia para incentivar seus jogadores ;)'
          },
          conditions: {
            title: 'Condições de conquista',
            description: 'Configure como os seus jogadores terão que fazer para ganhar esta insígnia.'
          }
        },
        buttons: {
          save: "Salvar",
          upload: "Carregar",
          delete: "Remover",
          addAction: "Ação",
          activated: "Ativo",
          inactivated: "Inativo"
        },
        fields: {
          file: "Imagem",
          name: "Nome da insígnia",
          bonusPoints: "Pontuação bonus",
          active: "Ativo ou inativo",
          conditions: {
            type: "Usuários ganham essa insígnia quando ",
            match: [
              { code: "all", description: "atingirem todas as condições."},
              { code: "any", description: "atingirem alguma das condições."}
            ],
            times_to_achieve: "Vezes"
          }
        },
        messages: {
          name: {
            required: "Você precisa informar o nome da insígnia" 
          },
          bonusPoints: {
            required: "Você precisa informar a pontuação bônus"
          },
          conditions: {
            minValue: "Você precisa informar a condição para ganhar a insígnia"
          },
          toast: {
            badges: {
              badgeWasSaved: {
                title: "A insígnia foi salva",
                text: "É isso aí ;)"
              },
              badgeWasNotSaved: {
                title: "Falha para salvar a insígnia",
                text: "Algo de errado =/"
              },
              badgeWasDeleted: {
                title: "A insígnia foi removida",
                text: "Tudo bem ;'("
              },
              badgeWasNotDeleted: {
                title: "Falha ao remover a insígnia",
                text: "Algo deu errado =/"
              }
            }
          }
        }
      }  
    };

    /**
     * Labels usadas em game levels
     *
     * @method _gameLevels
     * @return {Object} Objeto com as labels
     */
    var _gameLevels = function () {
      return {
        heading: 'Níveis',
        callouts: {
          basics: {
            title: 'Informações básicas',
            description: 'Configure o nível acompanhar o desempenho de seus jogadores ;)'
          }
        },
        buttons: {
          save: "Salvar",
          upload: "Carregar",
          delete: "Remover",
          activated: "Ativo",
          inactivated: "Inativo"
        },
        fields: {
          file: "Imagem",
          name: "Nome da nível",
          bonusPoints: "Pontuação necessária",
          active: "Ativo ou inativo"
        },
        messages: {
          name: {
            required: "Você precisa informar o nome da nível" 
          },
          bonusPoints: {
            required: "Você precisa informar a pontuação necessária."
          },
          toast: {
            levels: {
              levelWasSaved: {
                title: "O nível foi salvo",
                text: "É isso aí ;)"
              },
              levelWasNotSaved: {
                title: "Falha para salvar o nível",
                text: "Algo de errado =/"
              },
              levelWasDeleted: {
                title: "O nível foi removido",
                text: "Tudo bem ;'("
              },
              levelWasNotDeleted: {
                title: "Falha ao remover o nível",
                text: "Algo deu errado =/"
              }
            }
          }
        }
      }  
    };
    
    /**
     * Labels usadas em game settings
     *
     * @method _gameSettings
     * @return {Object} Objeto com as labels
     */
    var _gameSettings = function () {
      return {
        heading: 'Configurações',
        fields: {
          name: {
            name: "Nome",
            placeholder: "Coloque um nome legal para o jogo"
          },
          unique_name: {
            name: "Id único",
            placeholder: "Coloque um id único para referenciar seu jogo"
          },
          description: {
            name: "Objetivos",
            placeholder: "Fale sobre o propósito do game, qual objetivo de existir ele.",
          },
          file: "Imagem"
        },
        buttons: {
          save: 'Atualizar',
          delete: 'Remover',
          accessToken: "Meu token"
        },
        callouts: {
          basics: {
            title: 'Descrição',
            description: 'Você pode mudar algumas configurações do jogo (também removê-lo se não servir mais).'
          }
        },
        messages: {
          toast: {
            gameWasUpdated: {
              title: "O jogo foi atualizado",
              text: "Tudo certo ;)"
            },
            gameWasNotUpdated: {
              title: "Falha ao atualizar o jogo",
              text: "Infelizmente algo não está certo"
            },
            gameWasDeleted: {
              title: "O jogo foi removido",
              text: "Esse jogo acabou!"
            },
            gameWasNotDeleted: {
              title: "Falha ao remover o jogo",
              text: "Algo não ficou legal"
            }
          }
        }
      }
    };
    
    /**
     * Labels para o modal de new game
     *
     * @method _modalNewGame
     * @return {Object} Objeto com as labels
     */
    var _modalNewGame = function () {
      return {
        heading: 'Novo jogo',
        fields: {
          name: {
            name: "Nome",
            placeholder: "Coloque um nome legal para o jogo"
          },
          unique_name: {
            name: "Id único",
            placeholder: "Coloque um id único para referenciar seu jogo"
          },
          description: {
            name: "Objetivos",
            placeholder: "Fale sobre o propósito do game, qual objetivo de existir ele.",
          },
          file: "Imagem"
        },
        buttons: {
          save: "Salvar",
          cancel: "Cancelar"
        }, 
        messages: {
          toast: {
            gameWasSaved: {
              title: "O jogo foi salvo",
              text: "Um jogo novinho em folha"
            },
            gameWasNotSaved: {
              title: "Falha ao salvar o jogo",
              text: "Bahhh..."
            }
          }
        }
      }
    };

    var _modalNewToken = function () {
      return {
        heading: 'Token',
        fields: {
          token: "Seu token é..."
        },
        buttons: {
          close: "Fechar",
          change: "Alterar"
        }, 
        messages: {
          toast: {
            tokenWasSaved: {
              title: "O token foi salvo",
              text: "Yup"
            },
            tokenWasNotSaved: {
              title: "O token não foi salvo",
              text: "Mas que droga"
            }
          }
        }
      }
    };
    
    /**
     * Labels para o modal de delete game
     *
     * @method _modalNewGame
     * @return {Object} Objeto com as labels
     */
    var _modalDeleteGame = function () {
      return {
        buttons: {
          delete: "Remover",
          cancel: "Cancelar"
        },
        messages: {
          info: 'Você tem certeza do que está fazendo?',
          toast: {
            gameWasDeleted: {
              title: "O jogo foi removido",
              text: "Fim de jogo"
            },
            gameWasNotDeleted: {
              title: "Falha ao remover o jogo",
              text: "Huhu..."
            }
          }
        }
      }
    }
    
    /**
     * Labels para a página de login
     *
     * @method _login
     * @return {Object} Objeto com as labels
     */
    var _login = function () {
      return {
        heading: "Acesso",
        fields: {
          email: "Email",
          password: "Senha",
          remember: "Lembrar-me"
        },
        buttons: {
          signup: "Novo?",
          login: "Entrar",
          forgot: "Esqueceu senha?"
        },
        messages: {
          toast: {
            userWasLoggedIn: {
              title: "Acesso obtido!",
              text: "Vamos trabalhar."
            },
            userWasNotLoggedIn: {
              title: "Falha para acessar",
              text: "Algo errado..."
            }
          }
        }
      };
    }

    return {
      shared: _shared,
      
      signup: _signup,
      activation: _activation,
      passwordRecoverSender: _passwordRecoverSender,
      passwordRecoverForm: _passwordRecoverForm,
      editAccount: _editAccount,
      
      gameList: _gameList,
      
      gameOverview: _gameOverview,
      gamePlayers: _gamePlayers,
      gamePlayersDetail: _gamePlayersDetail,
      gamePlayersSandbox: _gamePlayersSandbox,
      gameActions: _gameActions,
      gameBadges: _gameBadges,
      gameLevels: _gameLevels,
      gameSettings: _gameSettings,
      
      modalNewGame: _modalNewGame,
      modalNewToken: _modalNewToken,
      modalDeleteGame: _modalDeleteGame,
      
      login: _login
    }
  }

  /**
   * Módulo de factory de labels da aplicação
   *
   * @module gamekeeper.labels
   */
  angular.module('gamekeeper.labels', [])
    .factory('labelsFactory', labelsFactory);
  
})();

class UsersController < ApplicationController
  # wrap_parameters include: User.attribute_names + [:password, :password_confirmation]

	def show
    respond_with User.find(params[:id]).to_hash(:basic)
  end

  def create
    permitted = user_params
    response = UsersHelper.signup permitted[:name],
                      permitted[:email], 
                      permitted[:password], 
                      permitted[:password_confirmation]
    render status: response.status, json: { message: response.message }  
  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes(user_params)

    respond_with @user.to_hash(:basic)
  end

private

  def user_params
    params.require(:user).permit(:id, :name, :email, :password, :password_confirmation)
  end

end


(function () {

  var fileReader = ['$q', function ($q) {
    var _onLoad = function(reader, deferred, scope) {
      return function () {
          scope.$apply(function () {
              deferred.resolve(reader.result);
          });
      };
    };

    var _onError = function (reader, deferred, scope) {
      return function () {
          scope.$apply(function () {
              deferred.reject(reader.result);
          });
      };
    };

    var _getReader = function(deferred, scope) {
      var reader = new FileReader();
      reader.onload = _onLoad(reader, deferred, scope);
      reader.onerror = _onError(reader, deferred, scope);
      return reader;
    };

    var _readAsDataURL = function (file, scope) {
      var deferred = $q.defer();

      var reader = _getReader(deferred, scope);
      reader.readAsDataURL(file);

      return deferred.promise;
    };

    return {
      readAsDataUrl: _readAsDataURL
    }
  }];
    
  angular.module('gamekeeper.utils.factories', [])
    .factory('fileReader', fileReader);

})();

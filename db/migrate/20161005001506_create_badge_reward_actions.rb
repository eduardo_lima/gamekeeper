class CreateBadgeRewardActions < ActiveRecord::Migration
  def change
    create_table :badge_reward_actions do |t|
      t.integer :times_to_achieve

      t.boolean :removed, default: false

      t.references :badge_reward_condition, index: true, foreign_key: true
      t.references :action, index: true, foreign_key: true
    end
  end
end

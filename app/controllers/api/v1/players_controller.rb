class API::V1::PlayersController < ApiController

  before_action :load_saved_game, :init_hash

  def index
    
    page = params[:page] - 1 || 0
    per_page = params[:per_page] || 10
    
    players = Player
                .where(game_id: @game.id, removed: false)
                .includes(:level, :player_badges)
                .offset(page * 10)
                .limit(per_page)
                .order('points_obtained DESC')

    @hash[:data] = players.present?? players.collect { |p| p.to_hash(:stats)} : []

    respond_with @hash
  end

  def show
    player = Player.find_by_game_id_and_social_media_id(@game.id, params[:social_id])

    @hash[:data] = player.present?? player.to_hash(:rewards) : []

    respond_with @hash
  end

  def create
    player = @game.players.build
    player.name = params[:name]
    player.social_media_id = params[:social_id]
    player.image = params[:image_url]
    player.remote_picture_url = params[:image_url] # eu amo o carrierwave

    begin
      if player.save then
        render json: { data: player.to_hash(:api) }, status: :ok 
        return
      end
    rescue => exception
      puts exception.inspect
      puts player.errors.inspect
      render text: "Os elementos de jogo não estão bem definidos".html_safe, status: :bad_request
    end
  end

  # Executa a ação do player no game
  # 
  # /:game/players/:player_id/perform
  # 
  #
  # :player_id ID do player em uma mídia social_media_id
  # :performed_action
  #   :verb Ação que é executada
  #   :object_properties
  #     :name Nome da propriedade do objeto
  #     :value Valor para a propriedade do objeto
  def perform

    player = @game.players.find_by_social_media_id(params[:player_id])

    performed_action = PerformedAction.new
    performed_action.player_id = player.id
    performed_action.action_id = @game.actions.find_by_verb(params[:performed_action][:verb]).id

    performed_action.save

    params[:performed_action][:object_properties].each do |p|

      # sai se não colocou valor
      next if p[:value].blank?

      found_property = ObjectProperty.find_by_name(p[:name])

      # sai se não encontrou a propriedade
      next if found_property.blank?

      performed_action_property = PerformedActionProperty.new
      performed_action_property.performed_action_id = performed_action.id
      performed_action_property.object_property_id = found_property.id
      performed_action_property.value = p[:value]

      performed_action.performed_action_properties << performed_action_property
    end if params[:performed_action][:object_properties].present? and params[:performed_action][:object_properties].is_a?(Array)

    performed_action.save

    render json: { message: "OK" }, status: :ok

  rescue => exception
    logger.debug "Exception: #{exception.inspect} - #{exception.backtrace.inspect}"
    render json: { message: "Houve um problema com o envio da ação do jogo. Verifique os elementos de gamificação."}, status: 422
  end

private

  def init_hash
    @hash = {}
  end

  def load_saved_game
    @game = Game.find_by_unique_name(params[:unique_name])
  end


  before_action only: :perform do
    unless params.has_key?('player_id') && params.has_key?('performed_action')
      render nothing: true, status: :bad_request
    end
  end
end
(function () {
  
  angular.module('gamekeeper.game.actions', [
    'gamekeeper.game.actions.controllers',
    'gamekeeper.game.actions.directives',
    'gamekeeper.game.actions.factories'
  ]);
  
})();
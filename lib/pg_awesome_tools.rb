# Módulo que contém as mágicas do multi-tenancy 
module PgAwesomeTools 
  # extends self

  # Retorna o schema atual 
  def self.search_path
    ActiveRecord::Base.connection.schema_search_path
  end

  # Retorna o schema padrão 
  def self.default_search_path
    @default_search_path ||= %{"$user", public}
  end

  # Define o schema do banco de dados atual
  # name -> Nome do schema a ser buscado
  # include_public -> flag para não incluir tabelas compartilhadas serem carregadas
  def self.set_search_path(name, include_public = true)
    path_parts = [name.to_s, ("public" if include_public)].compact
    ActiveRecord::Base.connection.schema_search_path = path_parts.join(",")
  end

  def self.restore_default_search_path
    ActiveRecord::Base.connection.schema_search_path = default_search_path
  end

  def self.create_schema(name)
    sql = %{CREATE SCHEMA "#{name}"}
    ActiveRecord::Base.connection.execute sql
  end

  def self.schemas
    sql = "SELECT nspname FROM pg_namespace WHERE nspname !~ '^pg_.*'"
    ActiveRecord::Base.connection.query(sql).flatten
  end

end
(function () {
  var gameDashboardFactory = ['$http', function ($http) {
    
    var _loadActionReport = function (gameId) {
      return $http.get('/games/' + gameId + '/dashboards/action_report.json');
    };

    var _loadBadgeReport = function (gameId) {
      return $http.get('/games/' + gameId + '/dashboards/badge_report.json');
    };

    var _loadLevelReport = function (gameId) {
      return $http.get('/games/' + gameId + '/dashboards/level_report.json');
    };
    
    return {
      loadActionReport: _loadActionReport,
      loadBadgeReport: _loadBadgeReport,
      loadLevelReport: _loadLevelReport
    }
  }];
  
  angular.module('gamekeeper.game.dashboards.factories', [])
    .factory('gameDashboardFactory', gameDashboardFactory);
})();

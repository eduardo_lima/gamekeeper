class API::V1::LevelsController < ApiController

  before_action :load_saved_game, :init_hash

  def index
    levels = Level.where(game_id: @game.id, active: true)

    @hash[:data] = levels.present?? levels.collect { |l| l.to_hash(:api)} : []

    respond_with @hash
  end

  def show
    level = Level.find_by_game_id_and_id_and_active(@game.id, params[:level_id], true)

    @hash[:data] = level.present?? level.to_hash(:api) : []

    respond_with @hash
  end

private

  def init_hash
    @hash = {}
  end

  def load_saved_game
    @game = Game.find_by_unique_name(params[:unique_name])
  end
end
Rails.application.routes.draw do
 
  root to: 'application#angular'

  get 'server_time' => 'api#server_time'
  
  post   'signup'       => 'users#create'
  get    'loggedIn'     => 'sessions#logged_in'
  post   'login'        => 'sessions#create'
  delete 'logout'       => 'sessions#destroy'

  resources :account_activations do
    member do
     post 'activate'
    end
  end

  resources :password_resets do
    member do
      post 'recover'
    end
  end

  resources :users
  resources :games do
    resources :actions
    resources :badges
    resources :levels
    resources :players do 
      member do 
        get  'show'
        get  'detail'
        post 'perform_some_action'
      end
    end
    resources :dashboards do 
      collection do
        get 'action_report'
        get 'badge_report'
        get 'level_report'
      end
    end

    member do
      put 'change_token'
    end
  end

  ##### NOW API ROUTES....

  namespace :api do
    namespace :v1 do
     
      # endpoints principal do game
      scope '/:unique_name' do
        get '/' => 'games#show'

        # endpoints das actions
        scope '/actions' do
          get '/' => 'actions#index'
          scope '/:verb' do
            get '/' => 'actions#show'
          end
        end

        # endpoints dos badges
        scope '/badges' do
          get '/' => 'badges#index'
          scope '/:badge_id' do
            get '/' => 'badges#show'          
          end
        end

        # endpoints dos levels
        scope '/levels' do
          get '/' => 'levels#index'
          scope '/:level_id' do
            get '/' => 'levels#show'          
          end
        end

        # endpoints dos players
        scope '/players' do
          get '/' => 'players#index'
          post '/' => 'players#create'
          scope '/:social_id' do
            get  '/' => 'players#show'
            post '/perform' => 'players#perform'
          end
        end

        # endpoints dos leaderboards
        scope '/leaderboards' do
          get '/overall_rankings' => 'dashboards#overall'
        end
      end
    end
  end

end

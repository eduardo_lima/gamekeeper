class CreatePerformedActionProperties < ActiveRecord::Migration
  def change
    create_table :performed_action_properties do |t|
      t.string :value
      t.references :performed_action, index: true, foreign_key: true
      t.references :object_property, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

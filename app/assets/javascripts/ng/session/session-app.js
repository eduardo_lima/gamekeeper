(function () {
  
  /**
   * Módulo de sessões do usuário
   *
   * @module sessions
   * @requires session.controllers Módulo com controllers
   * @requires session.factories Módulo com factories
   */
  angular.module('gamekeeper.sessions', [
    'gamekeeper.session.controllers', 
    'gamekeeper.session.factories'
  ]);
  
})();
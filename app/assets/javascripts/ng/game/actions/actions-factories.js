(function () {

  /**
   * Classe de game action factories, consiste em chamadas de backend e métodos auxiliares
   *
   * @namespace game.factories
   * @class gameActionFactory
   * @constructor
   * @param $http Serviço de requisições http
   * @param Upload Helper para upload de imagens
   */
  var gameActionFactory = ['$http', 'Upload', '$q', function ($http, Upload, $q) {
    
    var canceler;
    
    var _startFactory = function () {
      canceler = $q.defer(); 
    }
    
    var _stopFactory = function () {
      canceler.resolve('cancelled');
    }

    /**
     * Método para salvar os atributos do game action
     *
     * @method _internalSave
     * @param gameAction Objeto com os atributos do game
     * @param method Tipo de verbo HTTP (POST, PUT)
     * @param url Endpoint da aplicação de backend
     * @return {Object} Angular Promise
     */
    var _internalSave = function (gameAction, method, url) {
      var image = gameAction.file ? gameAction.file: [];
      var options = {
        url: url,
        method: method,
        file: image,
        file_form_data_name: gameAction.verb ? gameAction.verb: "",
        fields: {
          // traduzir para backend
          game_action: {
            game_id: gameAction.game_id,
            verb: gameAction.verb,
            past_tense: gameAction.past_tense,
            points: gameAction.points,
            bonus_points: gameAction.bonus_points,
            action_object_attributes: {
              article: gameAction.action_object.article,
              singular: gameAction.action_object.singular,
              plural: gameAction.action_object.plural
            },
            object_properties_attributes: gameAction.object_properties
          }
        }
      }    
      return Upload.upload(options);  
    }
    
    /**
     * Chama a aplicação back-end para salvar o game action
     *
     * @method _save
     * @param gameAction Objeto com os atributos do game action
     * @return {Object} Angular Promise
     */
    var _save = function (gameAction) {
      return _internalSave(gameAction, 'POST', '/games/' + gameAction.game_id + '/actions.json');
    }
    
    /**
     * Chama a aplicação back-end para atualizar o game action
     *
     * @method _update
     * @param gameAction Objeto com os atributos do game action
     * @return {Object} Angular Promise
     */
    var _update = function (gameAction) {
      return _internalSave(gameAction, 'PUT', '/games/' + gameAction.game_id + '/actions/' + gameAction.id + '.json');
    }
    
    /**
     * Chama a aplicação back-end para remover o game action
     *
     * @method _delete
     * @param game Objeto com os atributos do game action
     * @return {Object} Angular Promise
     */
    var _delete = function (gameAction) {
      return $http.delete('/games/' + gameAction.game_id + '/actions/' + gameAction.id + '.json', { timeout: canceler.promise });
    }
    
    /**
     * Chama a aplicação back-end para listar o game actions
     *
     * @method _list
     * @param gameId Id do game
     * @return {Object} Angular Promise
     */
    var _list = function (gameId) {
      return $http.get('/games/' + gameId + '/actions.json', { timeout: canceler.promise });
    }
    
    return {
      start: _startFactory,
      stop: _stopFactory,
      save: _save,
      update: _update,
      delete: _delete,
      list: _list
    }
    
  }];
  
    
  /**
   * Módulo de factories de game
   * 
   * @module game.factories
   */
  angular.module('gamekeeper.game.actions.factories', ['ngFileUpload'])
    .factory('gameActionFactory', gameActionFactory);

})();

class PasswordResetsController < ApplicationController
  include PasswordResetsHelper

  skip_before_filter :current_user, :only => [:create, :recover]

  # antes de recuperar a senha deve-se validar o usuário
  before_action :valid_user, only: [:reset]
  # antes de recuperar a senha deve-se verificar a expiração do token
  before_action :check_expiration, only: [:reset]

  # Método para permitir o usuário recuperar a senha
  # Retorna mensagem de email enviado ou não
  def create
    reply = request_recover_instructions(params[:email].downcase)
    render status: reply.status, json: { message: reply.message }
  end

  # Método para recuperar a senha do usuário
  # Retorna mensagem de recuperação realizada
  #
  # A validação de recuperar a senha é feita em halt
  def recover
    reply = request_recover_password(params[:id], user_params[:email], user_params[:password], user_params[:password_confirmation])
    save_session reply.params['auth_token'] if reply.success
    render status: reply.status, json: { message: reply.message }
  end

private

  # Obtem quais são os parâmetros pemitidos nas requisições
  # Retorna objeto com parâmetros permitidos.
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end


end

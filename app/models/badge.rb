class Badge < ActiveRecord::Base

  # um badge pertence a um game
  belongs_to :game
  has_one :badge_reward_condition, dependent: :destroy
  accepts_nested_attributes_for :badge_reward_condition
  
  # uma badge tem uma imagem de referencia
  has_attached_file :image, :styles => { :small => "150x150>" },
                  :default_url => "/assets/game-badge-icon.png",
                  :url  => "/assets/game_badges/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/game_badges/:id/:style/:basename.:extension"

  mount_uploader :picture, PictureUploader
  
  # o anexo da imagem tem que ser uma imagem
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  # o id da badge deve ser preenchido
  validates :game_id,
            :presence   => { :message => I18n.t('activerecord.badge.errors.messages.game_id.presence') }
  # o identificador da badge nao deve conter espacos
  validates :name,
            :presence   => { message: I18n.t('activerecord.badge.errors.messages.name.presence')},
            :length     => { maximum: 50, message: I18n.t('activerecord.badge.errors.messages.name.length')}
  # os points da badge devem ser informados
  validates :bonus_points,
            :presence   => { message: I18n.t('activerecord.badge.errors.messages.bonus_points.presence') } 
 
  # a badge deve ser unica no game
  validate  :must_be_unique

  validate  :must_have_at_least_badge_reward_action

  # Incrementar contador de badges no game
  after_create :increment_game_badges
  # Decrementar contadr de acions no game
  # after_destroy :decrement_game_badges
  
  # Converte os campos do model de acordo com a opcao determinada.
  # Retorna um hash com os campos do model.
  #
  # O parametro 'inc' define qual serao os campos incluidos no 
  # hash. Quando 'inc' for <tt>:all</tt> todos os campos devem 
  # estar no hash.
  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) then
      hash[:id] = self.id
      hash[:game_id] = self.game_id
      hash[:name] = self.name
      hash[:bonus_points] = self.bonus_points
      hash[:active] = self.active
      hash[:image_url] = self.picture.url.present?? self.picture.url : "game-badge-icon.png"
      hash[:badge_reward_condition] = self.badge_reward_condition.to_hash(:basic)

      conditions = self.badge_reward_condition.badge_reward_actions.where(removed: false)
      hash[:badge_reward_actions] = conditions.collect do |bra|
        bra.to_hash(:basic)
      end unless conditions.empty?
    end

    if inc.include?(:all) or inc.include?(:stats) then
      # soma badges distinguindo somente por jogador
      total_players = Player.where(game_id: self.game_id).count
      total_players_earned = PlayerBadge.select('player_id').distinct.where(badge_id: self.id).count

      hash[:players_earned] = total_players_earned
      hash[:players_earned_perc] =((total_players_earned.to_f / total_players.to_f) * 100).round(2) if total_players > 0
    end

    if inc.include?(:all) or inc.include?(:api) then
      hash[:id] = self.id
      hash[:name] = self.name
      hash[:bonus_points] = self.bonus_points
      hash[:image_url] = self.picture.url.present?? self.picture.url : "game-badge-icon.png"
      hash[:badge_reward_condition] = self.badge_reward_condition.condition_type
      hash[:badge_reward_actions] = {
        :data => self.badge_reward_condition.badge_reward_actions.collect { |bra| bra.to_hash(:api) }
      }
    end
    
    return hash
  end

  def remove_this
    self.removed = true
    self.save

    game = Game.find self.game.id
    game.decrement :number_of_badges
    game.save
  end

private

  def must_be_unique
    # procurar badges nesse game que tenham o mesmo verb
    game_badge = Badge.where(game_id: self.game_id, name: self.name).first

    # exibe erro quando achou o game e o game ter id diferente
    if !game_badge.nil? && self.id != game_badge.id
      errors.add(:name, I18n.t('activerecord.badge.errors.messages.name.uniqueness') )
    end
  end

  def must_have_at_least_badge_reward_action
    if self.badge_reward_condition.badge_reward_actions.blank? then
      errors.add(:badge_reward_condition, I18n.t('activerecord.badge.errors.messages.badge_reward_actions.presence'))
    end
  end

  # Incrementa o número de badges no game
  def increment_game_badges
    game = Game.find self.game.id
    game.increment :number_of_badges
    game.save
  end

  # # Decrementa o número de badge no game
  # def decrement_game_badges
  #   game = Game.find self.game.id
  #   game.decrement :number_of_badges
  #   game.save
  # end




end

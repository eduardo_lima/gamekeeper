require 'test_helper'

class GameTest < ActiveSupport::TestCase

  def setup
    @game = Game.new
  end
  
  # Testar basico para contabilizar
  test "should return true because it is a first test" do
    assert true
  end

  # Testar para ver se carregou as fixtures
  test "should return true if there are fixtures" do
    assert Game.all.count > 0
  end

  # Testar para nao salvar objeto invalido
  test "should not save a game without information" do
    assert_not @game.save
  end

  # Testar para salvar o jogo com as informacoes obrigatorias
  test "should save a new game" do
    @game.user_id = 1
    @game.name = Faker::Pokemon.name
    @game.unique_name = "game_#{Game.all.count + 1}"
    @game.objective = Faker::Hipster.sentence(10, true)
    assert @game.save
  end

  # Testar para nao salvar id unico repetido
  test "should not save @game when it has not unique_name" do
    @game.user_id = 1
    @game.name = Faker::Pokemon.name
    @game.unique_name = "game_1"
    @game.objective = Faker::Hipster.sentence(10, true)
    assert_not @game.save
  end

end

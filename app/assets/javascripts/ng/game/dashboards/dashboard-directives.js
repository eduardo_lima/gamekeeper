(function () {

  var gameDashboardsDirective = function () {
    return {
      templateUrl: "assets/views/games/directives/_dashboards-panel.html",
      scope: {
        game: "="
      },
      controller: 'GameDashboardCtrl'
    }  
  };

  angular.module('gamekeeper.game.dashboards.directives', [])
    .directive('gkGameDashboards', gameDashboardsDirective);
})();
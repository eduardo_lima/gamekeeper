class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.string :verb
      t.string :past_tense
      t.integer :points
      t.attachment :image

      t.boolean :hidden, default: false
      t.boolean :removed, default: false

      t.references :game, index: true, foreign_key: true
    end

    add_index :actions, :verb
  end
end

(function () {

  /**
   * Classe de session factories, consiste em chamadas de backend e métodos auxiliares
   *
   * @namespace session.factories
   * @class sessionFactory
   * @constructor
   * @param $http Serviço de requisições http
   */
  var sessionFactory = ['$http', function ($http) {
    
    var _login = function (credentials) {
      return $http.post('/login.json', credentials);
    }
    
    var _loggedIn = function () {
      return $http.get('/loggedIn.json');
    }
    
    var _logout = function () {
      return $http.delete('/logout.json');
    }
    
    return {
      login: _login,
      loggedIn: _loggedIn,
      logout: _logout
    }
  }];
    
  /**
   * Módulo de factories de session
   * 
   * @module session.factories
   */
  angular.module('gamekeeper.session.factories', [])
    .factory('sessionFactory', sessionFactory);

})();

(function () {
  
  /**
   * Classe de user factories, consiste em chamadas de backend e métodos auxiliares
   *
   * @namespace user.factories
   * @class accountFactory
   * @constructor
   * @param $http Serviço de requisições http
   */
  var accountFactory = ['$http', function ($http) {
    
    /**
     * Método para registar usuario no gamekeeper
     *
     * @method _signup
     * @param userInfo Dados do usuario
     * @return {Object} Angular Promise
     */
    var _signup = function (userInfo) {
      return $http.post('/signup.json', userInfo);
    }
    
    /**
     * Método para ativar conta de usuario no gamekeeper
     *
     * @method _activate
     * @param token Codigo de ativacao de conta
     * @param email Email que recebeu codigo de ativacao
     * @return {Object} Angular Promise
     */
    var _activate = function (token, email) {
      return $http.post('/account_activations/' + token + '/activate.json?email=' + email)
    }
    
    /**
     * Método para enviar email de recuperacao de senha
     *
     * @method _sendRecoverEmail
     * @param email Email que recebera codigo de recuperacao de senha
     * @return {Object} Angular Promise
     */
    var _sendRecoverEmail = function (email) {
      return $http.post('/password_resets.json?email=' + email);
    }
    
    /**
     * Método para recuperar senha 
     *
     * @method _recoverPassword
     * @param token Token de recuperacao de senha
     * @param user Objeto com a nova senha do usuario
     * @return {Object} Angular Promise
     */
    var _recoverPassword = function (token, user) {
      return $http.post('/password_resets/' + token + '/recover.json', {
        user: {
          email: user.email,
          password: user.password,
          password_confirmation: user.passwordConfirm
        }});
    }
    
    /**
     * Método para atualizar usuario no gamekeeper
     *
     * @method _update
     * @param userInfo Dados do usuario
     * @return {Object} Angular Promise
     */
    var _update = function (userInfo) {
      return $http.put('/users/' + userInfo.id + '.json', userInfo);
    }
    
    return {
      signup: _signup,
      activate: _activate,
      sendRecoverEmail: _sendRecoverEmail,
      recoverPassword: _recoverPassword,
      update: _update
    }
    
  }];
    
  /**
   * Módulo de factories de users
   * 
   * @module user.factories
   */
  angular.module('gamekeeper.user.factories', [])
    .factory('accountFactory', accountFactory);
  
})();

class Level < ActiveRecord::Base
  # um level pertence a um game
  belongs_to :game

  has_many :players
  
  # uma level tem uma imagem de referencia
  has_attached_file :image, :styles => { :small => "150x150>" },
                  :default_url => "/assets/game-level-icon.png",
                  :url  => "/assets/game_levels/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/game_levels/:id/:style/:basename.:extension"

  mount_uploader :picture, PictureUploader
  
  # o anexo da imagem tem que ser uma imagem
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  # o id da level deve ser preenchido
  validates :game_id,
            :presence   => { :message => I18n.t('activerecord.level.errors.messages.game_id.presence') }
  # o identificador da level nao deve conter espacos
  validates :name,
            :presence   => { message: I18n.t('activerecord.level.errors.messages.name.presence')},
            :length     => { maximum: 50, message: I18n.t('activerecord.level.errors.messages.name.length')}
  # os points da level devem ser informados
  validates :required_points,
            :presence   => { message: I18n.t('activerecord.level.errors.messages.required_points.presence') } 
 
  # a level deve ser unica no game
  validate  :must_be_unique

  # Incrementar contador de levels no game
  after_create :increment_game_levels
  # Decrementar contadr de acions no game
  # after_destroy :decrement_game_levels
  
  # Converte os campos do model de acordo com a opcao determinada.
  # Retorna um hash com os campos do model.
  #
  # O parametro 'inc' define qual serao os campos incluidos no 
  # hash. Quando 'inc' for <tt>:all</tt> todos os campos devem 
  # estar no hash.
  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) then
      hash[:id] = self.id
      hash[:game_id] = self.game_id
      hash[:name] = self.name
      hash[:required_points] = self.required_points
      hash[:active] = self.active
      hash[:image_url] = self.picture.url.present?? self.picture.url : "game-level-icon.png"
    end

    if inc.include?(:all) or inc.include?(:stats) then
      # soma badges distinguindo somente por jogador
      players = Player.where(game_id: self.game_id)
      total_players = players.count
      total_players_earned = players.select { |p| p.level_id == self.id }.count
      
      hash[:players_earned] = total_players_earned
      hash[:players_earned_perc] = ((total_players_earned.to_f / total_players.to_f) * 100).round(2) if total_players > 0
    end

    if inc.include?(:all) or inc.include?(:api) then
      hash[:id] = self.id
      hash[:name] = self.name
      hash[:required_points] = self.required_points
      hash[:image_url] = self.picture.url.present?? self.picture.url : "game-level-icon.png"
    end
    
    return hash
  end

  def remove_this
    self.removed = true
    self.save

    game = Game.find self.game.id
    game.decrement :number_of_levels
    game.save
  end

private

  def must_be_unique
    # procurar levels nesse game que tenham o mesmo verb
    game_level = Level.where(game_id: self.game_id, name: self.name).first

    # exibe erro quando achou o game e o game ter id diferente
    if !game_level.nil? && self.id != game_level.id
      errors.add(:name, I18n.t('activerecord.level.errors.messages.name.uniqueness') )
    end
  end

  # Incrementa o número de levels no game
  def increment_game_levels
    game = Game.find self.game.id
    game.increment :number_of_levels
    game.save unless self.hidden?
  end

  # Decrementa o número de level no game
  # def decrement_game_levels
  #   game = Game.find self.game.id
  #   game.decrement :number_of_levels
  #   game.save
  # end
end

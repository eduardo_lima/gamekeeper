class ApiController < ActionController::Base
  before_action :restrict_access
  respond_to :json

private

  def restrict_access
    api_key = APIKey.find_by_access_token_and_unique_name(params[:access_token], params[:unique_name])
    head :unauthorized unless api_key
  end

end
(function () {
  
  /**
   * Controller para a lista de games/projetos
   *
   * @namespace game.controllers
   * @class GameListCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $state Helper para controlar states
   * @param labels Objeto com labels específicas do escopo
   * @param sessionFactory Factory de sessoes do usuario
   * @param gameFactory Factory para CRUD de games
   * @param $uibModal Helper para criar modal Angular-bootstrap
   */
  var GameListCtrl = ['$scope', '$state', 'labels', 'miscLabels', 'sessionFactory', 'gameFactory', '$uibModal',
    function ($scope, $state, labels, miscLabels, sessionFactory, gameFactory, $uibModal) {
    
    // flag para quando estiver carregado os games
    $scope.gamesAreLoaded = false;

    // obtem todas as labels para renderizar no html
    $scope.labels = labels;
    $scope.miscLabels = miscLabels;

    gameFactory.list().then(function (response) {
      if (response.data.length > 0) {
        $scope.games = response.data;
      }
      $scope.gamesAreLoaded = true;
    });

    /**
      * @name newGame
      * @desc Abre modal para novo projeto
      * @memberOf Controllers.Home
    */
    $scope.newGame = function (size) {
      var modalInstance = $uibModal.open({
        templateUrl: 'assets/views/modals/_new-game.html',
        controller: 'ModalNewGameCtrl',
        size: size,
        resolve: {
          user: function () {
            return $scope.user;
          },
          labels: ['labelsFactory', function (labelsFactory) {
            return labelsFactory.modalNewGame();
          }]
        }
      });

      modalInstance.result.then(function () {
        gameFactory.list().then(function (response) {
          $scope.games = response.data;
        });
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      })
    };

  }];
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  

  /**
   * Controller para os projetos/game
   *
   * @namespace game.controllers
   * @class GameMenuCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $state Helper para controlar states
   * @param $stateParams Parâmetros definidos para o states
   * @param menu Objeto para menu selecionado
   * @param labels Objeto com labels específicas do escopo
   * @param miscLabels Objeto com labels compartilhadas na aplicação
   * @param game Objeto com os dados do game
   */
  var GameMenuCtrl = ['$scope', '$state', '$stateParams', 'menu', 'labels', 'miscLabels', 'game', 'user',
    function ($scope, $state, $stateParams, menu, labels, miscLabels, game, user) {
    
    $scope.id = $stateParams.id;
    // obtem labels especificas
    $scope.labels = labels;
    // obtem labels miscelaneas
    $scope.miscLabels = miscLabels;
    // obtem response com o projeto
    $scope.game = game.data;
    // Definir o menu selecionado
    $scope.selectedMenu = menu;
    $scope.user = user.data.user;

    $scope.isActiveMenu = function (supposedMenus) {
      return supposedMenus.indexOf($scope.selectedMenu) >= 0;
    }

  }];

  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  
  /**
   * Controller para o modal de criar games
   *
   * @namespace game.controllers
   * @class ModalNewGameCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $uibModalInstance Angular-Bootstrap widget
   * @param labels Objeto com as labels do modal
   * @param gameFactory Factory para salvar o game
   * @param fileReader Faz a leitura de um arquivo de imagem
   * @param toastr Mensagens de feedback toast
   */
  var ModalNewGameCtrl = ['$scope', '$uibModalInstance', 'labels', 'gameFactory', 'toastr', 'fileReader', 'user',
    function ($scope, $uibModalInstance, labels, gameFactory, toastr, fileReader, user) {
    
    $scope.labels = labels;
    $scope.isSavingGame = false;

    $scope.game = {
      user_id: user.id,
      name: '',
      unique_name: '',
      objective: '',
      image_url: '/assets/game-icon.png'
    }

    $scope.save = function (game) {

      $scope.isSavingGame = true;

      gameFactory.save(game).then(function (response) {

        toastr.success($scope.labels.messages.toast.gameWasSaved.text, $scope.labels.messages.toast.gameWasSaved.title);

        $uibModalInstance.close('save');

      }).catch(function (response) {
        // isso é um unprocessable entity
        if (response.status == 422) {
          var attribute = Object.keys(response.data.errors)[0];
          var values = response.data.errors[attribute]; // vem em array cada conjunto de erros do rails (ou angular, sei lá)
          values.forEach(function (e, i, a) {
            toastr.error(e, $scope.labels.messages.toast.gameWasNotSaved.title);
          });
        } else {
          toastr.error($scope.labels.messages.toast.gameWasNotSaved.text, $scope.labels.messages.toast.gameWasNotSaved.title);
        }

        $scope.isSavingGame = false;
      });
    };

    $scope.cancel = function () {
      console.log('cancel');
      $scope.game = {};
      $uibModalInstance.dismiss('cancel');
    };

    $scope.getFile = function () {
        fileReader.readAsDataUrl($scope.game.file, $scope).then(function(result) {
          $scope.game.image_url = result;
        });
    };
  }];

  var ModalNewToken = ['$scope', '$uibModalInstance', 'labels', 'gameFactory', 'toastr', 'game',
    function ($scope, $uibModalInstance, labels, gameFactory, toastr, game) {

    $scope.labels = labels;
    $scope.loading = false;
    $scope.game = game;

    $scope.change = function (game) {
      $scope.loading = true;

      gameFactory.changeToken(game).then(function (response) {

        toastr.success($scope.labels.messages.toast.tokenWasSaved.text, $scope.labels.messages.toast.tokenWasSaved.title);

        $scope.game.access_token = response.data.token;

        $scope.loading = false;

      }).catch(function (response) {

        toastr.error($scope.labels.messages.toast.tokenWasNotSaved.text, $scope.labels.messages.toast.tokenWasNotSaved.title);

        $scope.loading = false;
      });
    };

    $scope.cancel = function () {
      console.log('cancel');
      $uibModalInstance.dismiss('cancel');
    };
  }];
  
   
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------

  
  /**
   * Controller para os settings do game
   *
   * @namespace game.controllers
   * @class GameSettingsCtrl
   * @constructor
   * @param $scope Escopo Angular
   * @param $uibModal Angular-Bootstrap widget
   * @param $state Helper para controlar states
   * @param labelsFactory Labels para os componentes da página
   * @param toastr Mensagens de feedback toast
   */
  var GameSettingsCtrl = ['$scope', '$uibModal', '$state', '$ngBootbox', 'gameFactory', 'labelsFactory', 'toastr', 'fileReader', 
    function ($scope, $uibModal, $state, $ngBootbox, gameFactory, labelsFactory, toastr, fileReader) {

      // carrega as labels dos componentes da html
      $scope.labels = labelsFactory.gameSettings();
      // nada processando agora
      $scope.isSettingsProcessing = false;

      $scope.newToken = function (size) {
        $uibModal.open({
          templateUrl: 'assets/views/modals/_new-token.html',
          controller: 'ModalNewToken',
          size: size,
          resolve: {
            game: function () {
              return $scope.game;
            },
            labels: ['labelsFactory', function (labelsFactory) {

              return labelsFactory.modalNewToken();
            }]
          }
        });
      };

      $scope.update = function (game) {

        $scope.isSettingsProcessing = true;

        gameFactory.update(game).then(function (response) {

          toastr.success($scope.labels.messages.toast.gameWasUpdated.text, $scope.labels.messages.toast.gameWasUpdated.title);

        }).catch(function (response) {
          // isso é um unprocessable entity
          if (response.status == 422) {
            var attribute = Object.keys(response.data.errors)[0];
            var values = response.data.errors[attribute]; // vem em array cada conjunto de erros do rails (ou angular, sei lá)
            values.forEach(function (e, i, a) {
              toastr.error(e, $scope.labels.messages.toast.gameWasNotUpdated.title);
            });
          } else {
            toastr.error($scope.labels.messages.toast.gameWasNotUpdated.text, $scope.labels.messages.toast.gameWasNotUpdated.title);
          }
        }).finally(function () {
          $scope.isSettingsProcessing = false;
        });
      };

      $scope.delete = function (game) {

        $ngBootbox.confirm('Are you sure about this? This cannot be undone.').then(function() {

          $scope.isDeletingGame = true;

          gameFactory.delete(game).then(function (response) {
            toastr.success($scope.labels.messages.toast.gameWasDeleted.text, $scope.labels.messages.toast.gameWasDeleted.title);
            $state.go('app.game.list');
          }).catch(function (response) {
            toastr.error($scope.labels.messages.toast.gameWasNotDeleted.text, $scope.labels.messages.toast.gameWasNotDeleted.title);
          }).finally(function () {
            $scope.isDeletingGame = false;
          });

        }, function() {
          console.log('Confirm dismissed!');
        });
      }

      $scope.getFile = function () {
        fileReader.readAsDataUrl($scope.game.file, $scope).then(function(result) {
          $scope.game.image_url = result;
        });
    };
  }];

  /**
   * Faz bind do evento change para upload de arquivos
   *
   * @namespace game.controllers
   * @class gameImageSelect
   * @constructor
   */
  var gameImageSelect = function () {
    return {
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind("change", function(e){
          scope.game.file = (e.srcElement || e.target).files[0];
          scope.getFile();
        });
      }
    }
  }
   
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
      
  /**
   * Módulo de controllers de game
   * 
   * @module game.controllers
   */
  angular.module('gamekeeper.game.controllers', ['ngFileUpload'])
    .controller('GameListCtrl', GameListCtrl)
    .controller('GameMenuCtrl', GameMenuCtrl)
    .controller('ModalNewGameCtrl', ModalNewGameCtrl)
      .directive('gameImageSelect', gameImageSelect)
    .controller('GameSettingsCtrl', GameSettingsCtrl)
    .controller('ModalNewToken', ModalNewToken);

})();

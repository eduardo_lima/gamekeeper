class ActionObject < ActiveRecord::Base
  # um objeto pertence a uma ação
	belongs_to :action
  # um objeto tem propriedades
  has_many :object_properties, dependent: :destroy

  # o artigo do objeto é obrigatoria
	validates :article,
            :presence => { :message => I18n.t('activerecord.action_object.errors.messages.article.presence') },
            :length   => { :maximum => 10, :message => I18n.t('activerecord.action_object.errors.messages.article.length') }
  # o substantivo no singular
	validates :singular,
            :presence => { :message => I18n.t('activerecord.action_object.errors.messages.singular.presence') },
            :length   => { :maximum => 50, :message => I18n.t('activerecord.action_object.errors.messages.singular.presence') }
  # o subistantivo no plural
  # validates :plural,
  #           :presence => { :message => I18n.t('activerecord.action_object.errors.messages.plural.presence') },
  #           :length   => { :maximum => 50, :message => I18n.t('activerecord.action_object.errors.messages.plural.presence') }


  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}

    if inc.include?(:all) or inc.include?(:api) then
      hash[:article] = self.article
      hash[:singular] = self.singular
      hash[:plural] = self.plural
    end
    
    return hash
  end
end

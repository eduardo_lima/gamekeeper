class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.string :social_media_id
      t.integer :points_obtained, default: 0
      t.attachment :image

      t.boolean :removed, default: false

      t.references :game, index: true, foreign_key: true
      t.references :level, index: true, foreign_key: true
    end
  end
end

require 'test_helper'

class GamesControllerTest < ActionController::TestCase

  # Retorna token da sessão ao autenticar usuário no Gatekeeper
  setup do
    reply_data = GatekeeperService.new.signin("edu.licap@gmail.com", "ukitaki123").params
    session[:auth_token] = reply_data['auth_token']

    @user = User.new(GatekeeperService.new.current_user(session[:auth_token]).params)
  end

  # Testa se o usuário logado está mostrando todos os games dele
  test "should get index" do
    hash_body = better_action_call :get, :index
    assert_response :success
    assert hash_body.count > 0
  end

  # Testa se está criando os games
  test "should create game" do
    assert_difference -> { Game.where(user_id: @user.id).count }  do
      better_action_call :post, :create, game: { user_id: @user.id,
                                                name: Faker::Pokemon.name,
                                                unique_name: "game_#{ Game.where(user_id: @user.id).count + 1}",
                                                objective: Faker::Hipster.sentence(10, true), 
                                                number_of_players: 0,
                                                number_of_actions: 0,
                                                number_of_badges: 0,
                                                number_of_levels: 0 } 
    end
  end

  # test "should show game" do
  #   get :show, id: @game
  #   assert_response :success
  # end

  # test "should get edit" do
  #   get :edit, id: @game
  #   assert_response :success
  # end

  # test "should update game" do
  #   patch :update, id: @game, game: {  }
  #   assert_redirected_to game_path(assigns(:game))
  # end

  # test "should destroy game" do
  #   assert_difference('Game.count', -1) do
  #     delete :destroy, id: @game
  #   end

  #   assert_redirected_to games_path
  # end
end

class API::V1::DashboardsController < ApiController

  before_action :load_saved_game, :init_hash

  def overall
  
    page = params[:page] - 1 || 0
    per_page = params[:per_page] || 10
    
    players = Player
                .where(game_id: @game.id, removed: false)
                .includes(:level, :player_badges)
                .offset(page * 10)
                .limit(per_page)
                .order('points_obtained DESC')

    @hash[:data] = players.present?? players.collect { |p| p.to_hash(:stats)} : []

    respond_with @hash
  end

private

  def init_hash
    @hash = {}
  end

  def load_saved_game
    @game = Game.find_by_unique_name(params[:unique_name])
  end
end
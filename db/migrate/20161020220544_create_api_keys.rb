class CreateAPIKeys < ActiveRecord::Migration
  def change
    create_table :api_keys do |t|
      t.string :access_token
      t.string :unique_name
    end

    add_index :api_keys, :unique_name
  end
end

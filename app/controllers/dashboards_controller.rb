class DashboardsController < ApplicationController

  def action_report
    respond_with PerformedAction.joins(:action).select(:action_id).distinct.where('actions.game_id = ? AND DATE(created_at) >= ?', params[:game_id], 7.days.ago.to_date).collect { |pa| pa.to_hash(:seven_days_stats)}
  end

  def badge_report
    respond_with Badge.where(game_id: params[:game_id], active: true).collect { |b| b.to_hash([:basic, :stats])}.reverse
  end

  def level_report
    respond_with Level.where(game_id: params[:game_id], active: true).collect { |l| l.to_hash([:basic, :stats])}.reverse
  end
end

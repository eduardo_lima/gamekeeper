(function () {

  var gameBadgeFactory = ['$http', 'Upload', '$q', function ($http, Upload, $q) {
    
    var canceler;
    
    var _startFactory = function () {
      canceler = $q.defer(); 
    }
    
    var _stopFactory = function () {
      canceler.resolve('cancelled');
    }

    var _internalSave = function (gameBadge, method, url) {
      var image = gameBadge.file ? gameBadge.file: [];
      var options = {
        url: url,
        method: method,
        file: image,
        timeout: canceler.promise,
        file_form_data_name: gameBadge.name ? gameBadge.name: "",
        fields: {
          // traduzir para backend
          game_badge: {
            game_id: gameBadge.game_id,
            name: gameBadge.name,
            bonus_points: gameBadge.bonus_points,
            active: gameBadge.active,
            conditions_attributes: gameBadge.badge_reward_condition,
            badge_reward_actions: gameBadge.badge_reward_actions
          }
        }
      }
      return Upload.upload(options);  
    }
    
    var _save = function (gameBadge) {
      return _internalSave(gameBadge, 'POST', '/games/' + gameBadge.game_id + '/badges.json');
    }
    
    var _update = function (gameBadge) {
      return _internalSave(gameBadge, 'PUT', '/games/' + gameBadge.game_id + '/badges/' + gameBadge.id + '.json');
    }
    
    var _delete = function (gameBadge) {
      return $http.delete('/games/' + gameBadge.game_id + '/badges/' + gameBadge.id + '.json', { timeout: canceler.promise });
    }
    
    var _list = function (gameId) {
      return $http.get('/games/' + gameId + '/badges.json', { timeout: canceler.promise });
    }
    
    return {
      start: _startFactory,
      stop: _stopFactory,
      save: _save,
      update: _update,
      delete: _delete,
      list: _list
    }
  }];
  
  angular.module('gamekeeper.game.badges.factories', ['ngFileUpload'])
    .factory('gameBadgeFactory', gameBadgeFactory);

})();

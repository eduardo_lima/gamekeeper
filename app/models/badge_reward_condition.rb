class BadgeRewardCondition < ActiveRecord::Base
  belongs_to :badge
  has_many :badge_reward_actions, dependent: :destroy

  accepts_nested_attributes_for :badge_reward_actions

  validates :condition_type,
            :presence   => { :message => I18n.t('activerecord.badge_reward_condition.errors.messages.condition_type.presence') }

  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) then
      hash[:condition_type] = self.condition_type
    end
    
    return hash
  end


end


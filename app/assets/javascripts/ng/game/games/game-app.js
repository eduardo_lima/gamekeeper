(function () {
  
  /**
   * Inicializador do módulo de games
   *
   * @namespace gamekeeper.game
   * @class Run
   * @constructor
   * @param $rootScope Escopo raiz do módulo
   */
  var Run = ['$rootScope', 'gameActionFactory',function ($rootScope, gameActionFactory) {
    
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      
      switch (fromState.name) {
        case 'app.game.actions':
            gameActionFactory.stop();
            break;
      }

    });
    
  }];
  
  /**
   * Módulo de configuração dos states da aplicacão
   *
   * @module game
   * @requires game.controllers Módulo com controllers
   * @requires game.factories Módulo com factories
   * @requires game.directives Módulo com directives
   */
  angular.module('gamekeeper.game', [
    'gamekeeper.game.controllers',
    'gamekeeper.game.actions',
    'gamekeeper.game.badges',
    'gamekeeper.game.levels',
    'gamekeeper.game.players',
    'gamekeeper.game.dashboards',
    'gamekeeper.game.factories',
    'gamekeeper.game.directives'
  ]).run(Run);
  
})();
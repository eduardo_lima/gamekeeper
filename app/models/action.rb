class Action < ActiveRecord::Base
  
  # uma action pertence a um game
  belongs_to :game
  # uma action tem um objeto de interacao
  has_one :action_object, dependent: :destroy
  # permite definir o objeto do tipo action_object 
  accepts_nested_attributes_for :action_object
  # uma action tem uma imagem de referencia
  has_attached_file :image, :styles => { :small => "150x150>" },
                  :default_url => "/assets/game-action-icon.png",
                  :url  => "/assets/game_actions/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/game_actions/:id/:style/:basename.:extension"

  mount_uploader :picture, PictureUploader
  
  # o anexo da imagem tem que ser uma imagem
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  # o id da action deve ser preenchido
  validates :game_id,
            :presence   => { :message => I18n.t('activerecord.action.errors.messages.game_id.presence') }
  # o identificador da action nao deve conter espacos
  VALID_VERB_REGEX = /\A[a-z0-9\-_]+\z/
  validates :verb,
            :presence   => { :message => I18n.t('activerecord.action.errors.messages.verb.presence') },
            :format     => { :with => VALID_VERB_REGEX, :message => I18n.t('activerecord.action.errors.messages.verb.format') },
            :length     => { :maximum => 50, :message => I18n.t('activerecord.action.errors.messages.verb.length') }
  # o verbo no passado da action deve estar presente
  validates :past_tense,
            :presence   => { :message => I18n.t('activerecord.action.errors.messages.past_tense.presence') },
            :length     => { :maximum => 50, :message => I18n.t('activerecord.action.errors.messages.past_tense.length') }
  # os points da action devem ser informados
  validates :points,
            :presence   => { :message => I18n.t('activerecord.action.errors.messages.points.presence') } 
  # o objeto da action deve ser informado
  validate  :must_have_game_object
  # a action deve ser unica no game
  validate  :must_be_unique

  # Incrementar contador de actions no game
  after_create :increment_game_actions
  # Decrementar contadr de acions no game
  # after_destroy :decrement_game_actions


  has_many :performed_actions, dependent: :destroy

  has_many :badge_reward_actions, dependent: :destroy
  
  # Converte os campos do model de acordo com a opcao determinada.
  # Retorna um hash com os campos do model.
  #
  # O parametro 'inc' define qual serao os campos incluidos no 
  # hash. Quando 'inc' for <tt>:all</tt> todos os campos devem 
  # estar no hash.
  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) then
      hash[:id] = self.id
      hash[:game_id] = self.game_id
      hash[:verb] = self.verb
      hash[:past_tense] = self.past_tense
      hash[:points] = self.points
      hash[:image_url] = self.picture.url.present?? self.picture.url : "game-action-icon.png"
      hash[:action_object] = self.action_object

      properties = []
      properties = self.action_object.object_properties.where(removed: false)
      hash[:object_properties] = properties unless properties.empty?
    end

    if inc.include?(:all) or inc.include?(:api) then
      hash[:id] = self.id
      hash[:verb] = self.verb
      hash[:past_tense] = self.past_tense
      hash[:points] = self.points
      hash[:image_url] = self.picture.url.present?? self.picture.url : "game-action-icon.png"
      hash[:action_object] = self.action_object.to_hash(:api)
      hash[:object_properties] = {
        :data => self.action_object.object_properties.collect { |op| op.to_hash(:api) }
      } unless self.action_object.object_properties.empty?
    end
    
    return hash
  end

  def remove_this
    self.removed = true
    self.save

    game = Game.find self.game.id
    game.decrement :number_of_actions
    game.save
  end

private

  # Valida se existe algum verb dentro do game antes de salvar.
  # Adiciona erro ao model
  def must_be_unique
    # procurar actions nesse game que tenham o mesmo verb
    game_action = Action.where(game_id: self.game_id, verb: self.verb).first

    # exibe erro quando achou o game e o game ter id diferente
    if !game_action.nil? && self.id != game_action.id
      errors.add(:verb, I18n.t('activerecord.action.errors.messages.verb.uniqueness') )
    end
  end

  # Valida se a action tem objeto relacionado.
  # Adiciona erro no model
  def must_have_game_object
    if self.action_object.nil?
      errors.add(:action_object, I18n.t('activerecord.action.errors.messages.object.presence'))
    end
  end

  # Incrementa o número de actions no game
  def increment_game_actions
    game = Game.find self.game.id
    game.increment :number_of_actions
    game.save unless self.hidden?
  end

  # Decrementa o número de action no game
  # def decrement_game_actions
  #   game = Game.find self.game.id
  #   game.decrement :number_of_actions
  #   game.save
  # end


end

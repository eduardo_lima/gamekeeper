(function () {
  
  angular.module('gamekeeper.game.players', [
    'gamekeeper.game.players.controllers',
    'gamekeeper.game.players.directives',
    'gamekeeper.game.players.factories'
  ]);
  
})();
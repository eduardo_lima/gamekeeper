(function () {

  var gamePlayerFactory = ['$http', 'Upload', '$q', function ($http, Upload, $q) {
    
    var canceler;
    
    var _startFactory = function () {
      canceler = $q.defer(); 
    }
    
    var _stopFactory = function () {
      canceler.resolve('cancelled');
    }
    
    var _list = function (gameId, options) {

      return $http.get('/games/' + gameId + '/players.json', {
        params: {
          page: options.page
        }, 
        timeout: canceler.promise 
      });
    }

    var _find = function (gameId, playerId) {
      return $http.get('/games/' + gameId + '/players/' + playerId + '/show.json', { timeout: canceler.promise });
    }
    
    var _getPlayerDetail = function (gameId, playerId) {
      return $http.get('/games/' + gameId + '/players/' + playerId + '/detail.json', { timeout: canceler.promise });
    }

    var _performAction = function (gameId, playerId, performedAction) {
      return $http.post('/games/' + gameId + '/players/' + playerId + '/perform_some_action.json', { performed_action: performedAction });
    }
    
    return {
      list: _list,
      find: _find,
      getPlayerDetail: _getPlayerDetail,
      start: _startFactory,
      stop: _stopFactory,

      performAction: _performAction
    }
  }];
  
  angular.module('gamekeeper.game.players.factories', [])
    .factory('gamePlayerFactory', gamePlayerFactory);

})();

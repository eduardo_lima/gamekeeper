(function () {
  
  angular.module('gamekeeper.game.dashboards', [
    'gamekeeper.game.dashboards.controllers',
    'gamekeeper.game.dashboards.directives',
    'gamekeeper.game.dashboards.factories'
  ]);
  
})();
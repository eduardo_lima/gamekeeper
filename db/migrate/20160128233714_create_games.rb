class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :name
      t.string :unique_name, index: true
      t.string :objective
      t.integer :number_of_players, default: 0
      t.integer :number_of_actions, default: 0
      t.integer :number_of_badges, default: 0
      t.integer :number_of_levels, default: 0
      t.integer :user_id, index: true
      t.attachment :image

      t.boolean :removed, default: false

      t.timestamps null: false
    end
  end
end

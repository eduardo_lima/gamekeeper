
(function () {
  
  /**
   * Diretiva da Navbar do gamekeeper
   *
   * @namespace utils.directives
   * @class gkNavBar
   * @constructor
   */
  var gkNavBar = function () {
    return {
      restrict: 'E',
      templateUrl: 'assets/views/common/_navbar.html',
      scope: {
        user: "="
      },
      controller : 'NavbarController'
    }
  };
  
  /**
   * Diretiva da animacao de scroll up
   *
   * @namespace utils.directives
   * @class gkScrollUp
   * @constructor
   */
  var gkScrollUp = function ($anchorScroll) {
    return {
      restrict: 'A',
      transclude: true,
      replace: true,
      controller : function($scope){
        this.scrollToTop = function(){
          $('html, body').animate({scrollTop : 0}, 900);
        }
      },
      link : function(scope, element, attrs, ctrl){
        ctrl.scrollToTop();
      }
    }
  }
  
  /**
   * Diretiva para forcar numeros no input
   *
   * @namespace utils.directives
   * @class gkuiNumber
   * @constructor
   */
  var gkuiNumber = function ($filter) {
    return {
      require: "ngModel",
      link: function (scope, element, attrs, ctrl) {
        var _formatNumber = function (number) {
          number = number.replace(/[^0-9]+/g, "");
          return number;
        };

        element.bind("keyup", function () {
          ctrl.$setViewValue(_formatNumber(ctrl.$viewValue));
          ctrl.$render();
        });
      }
    };
  }

  /**
   * Módulo de diretivas uteis em toda aplicacao
   *
   * @module utils
   * @requires users.controllers Módulo com controllers
   * @requires users.factories Módulo com factories
   */  
  angular.module('gamekeeper.utils.directives', [])
    .directive('gkNavbar', gkNavBar)
    .directive('gkScrollUp', gkScrollUp)
    .directive('gkuiNumber', gkuiNumber);
})();

class User

  attr_accessor :id, :email, :name, :auth_token

  def initialize hash
    self.id = hash['id']
    self.email = hash['email']
    self.name = hash['name']
    self.auth_token = hash['auth_token']
  end

end
(function () {
  
  var GameLevelCtrl = ['$scope', 'fileReader', 'labelsFactory', 'gameLevelFactory', 'toastr', 'serialGenerator',
    function ($scope, fileReader, labelsFactory, gameLevelFactory, toastr, serialGenerator) {

    // carrega as labels dos componentes da html
    $scope.labels = labelsFactory.gameLevels();
    // carrega as levelss cadastradas no game
    $scope.gameLevels = []; // lembrar do serviço

    $scope.levelsAreLoaded = false;
    
    gameLevelFactory.start();
    
    // ############ LISTAR
    gameLevelFactory.list($scope.game.id).then(function (results) {
      $scope.gameLevels = results.data;

      $scope.levelsAreLoaded = true;
    });

    // ############ ADICIONAR
    $scope.new = function () {

      // aqui será configurado de como a levels será representada no form
      $scope.gameLevel = {
        id: undefined,
        isSelected: true,
        game_id: $scope.game.id,
        // inicializar a imagem
        image_url: '/assets/game-level-icon.png',
        name: '',
        required_points: '',
        active: true
      };

      // iterar e desselecionar tudo
      $scope.gameLevels.find(function (e, i, a) {
        e.isSelected = false;
      })

      $scope.gameLevels.push($scope.gameLevel);
    };
      
    // ############ SELECIONAR
    $scope.select = function (gameLevel) {
      // iterar e desselecionar tudo
      $scope.gameLevels.find(function (e, i, a) {
        e.isSelected = false;
      })

      // selecionar agora =)
      $scope.gameLevel = gameLevel;
      $scope.gameLevel.isSelected = true;
    };

    // ############ ANEXAR IMAGEM
    $scope.getFile = function () {
        fileReader.readAsDataUrl($scope.gameLevel.file, $scope).then(function(result) {
          $scope.gameLevel.image_url = result;
        });
    };
  
    // ############ ARMAZENAR levels
    $scope.save = function (gameLevel) {

      $scope.islevelProcessing = true;

      // ver se é edição ou criação, colocar condição aqui
      var promise = (gameLevel.id)? gameLevelFactory.update(gameLevel) : gameLevelFactory.save(gameLevel);

      promise.then(function (response) {

        // lembrar de setar o id para o objeto gamelevel
        gameLevel.id = response.data.id;

        toastr.success($scope.labels.messages.toast.levels.levelWasSaved.text, $scope.labels.messages.toast.levels.levelWasSaved.title);

        $scope.islevelProcessing = false;

      }).catch(function (response) {
        // isso é um unprocessable entity
        if (response.status == 422) {
          var attribute = Object.keys(response.data.errors)[0];
          var values = response.data.errors[attribute]; // vem em array cada conjunto de erros do rails (ou angular, sei lá)
          values.forEach(function (e, i, a) {
            toastr.error(e, $scope.labels.messages.toast.levels.levelWasNotSaved.title);
          });
        } else {
          toastr.error($scope.labels.messages.toast.levels.levelWasNotSaved.text, $scope.labels.messages.toast.levels.levelWasNotSaved.title);
        }

        $scope.islevelProcessing = false;
      });
    }

    // ############ REMOVER levels
    $scope.delete = function (gameLevel) {

      $scope.islevelProcessing = true;

      gameLevelFactory.delete(gameLevel).then(function () {
        // remover aqui a levels da lista
        $scope.gameLevels.find(function (e, i, a) {
          if (e && e.id == gameLevel.id) {
            $scope.gameLevels.splice(i, 1);
            toastr.success($scope.labels.messages.toast.levels.levelWasDeleted.text, $scope.labels.messages.toast.levels.levelWasDeleted.title);

            $scope.gameLevel = undefined; // não tem mais a levels selecionada
            $scope.islevelProcessing = false;
          }
        })
      }).catch(function (errors) {
        toastr.error($scope.labels.messages.toast.levels.levelWasNotDeleted.text, $scope.labels.messages.toast.levels.levelWasNotDeleted.title);

        $scope.islevelProcessing = false;
      });
    };
  }];
   
  /**
  * Faz bind do evento change para upload de arquivos
  *
  * @namespace game.controllers
  * @class gameLevelsImageSelect
  * @constructor
  */
  var gameLevelImageSelect = function () {
    return {
      restrict: 'A',
      link: function(scope, element, attrs){
        // se eu selecionar o mesmo arquivo em duas levelss diferentes, terá problema #bug
        element.bind("change", function(e){
          scope.gameLevel.file = (e.srcElement || e.target).files[0];
          scope.getFile();
        });
      }   
    }
  };
  
  /**
   * Módulo de controllers de game levelss
   * 
   * @module gamekeeper.game.levelss.controllers
   */
  angular.module('gamekeeper.game.levels.controllers', [])
    .controller('GameLevelCtrl', GameLevelCtrl)
      .directive('gameLevelImageSelect', gameLevelImageSelect)
})();

class API::V1::GamesController < ApiController

  def show
    hash = {}
    hash[:data] = Game.find_by_unique_name(params[:unique_name]).to_hash([:api, :stats]) 
    respond_with hash
  end
end
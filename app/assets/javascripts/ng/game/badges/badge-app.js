(function () {
  
  angular.module('gamekeeper.game.badges', [
    'gamekeeper.game.badges.controllers',
    'gamekeeper.game.badges.directives',
    'gamekeeper.game.badges.factories'
  ]);
  
})();
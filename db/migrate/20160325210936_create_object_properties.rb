class CreateObjectProperties < ActiveRecord::Migration
  def change
    create_table :object_properties do |t|
      t.string :name
      t.string :kind
      t.boolean :removed, default: false
      t.references :action_object, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

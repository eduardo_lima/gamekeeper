# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161111011648) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_objects", force: :cascade do |t|
    t.string  "article"
    t.string  "singular"
    t.string  "plural"
    t.integer "action_id"
  end

  add_index "action_objects", ["action_id"], name: "index_action_objects_on_action_id", using: :btree

  create_table "actions", force: :cascade do |t|
    t.string   "verb"
    t.string   "past_tense"
    t.integer  "points"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "hidden",             default: false
    t.boolean  "removed",            default: false
    t.integer  "game_id"
    t.string   "picture"
  end

  add_index "actions", ["game_id"], name: "index_actions_on_game_id", using: :btree
  add_index "actions", ["verb"], name: "index_actions_on_verb", using: :btree

  create_table "api_keys", force: :cascade do |t|
    t.string "access_token"
    t.string "unique_name"
  end

  add_index "api_keys", ["unique_name"], name: "index_api_keys_on_unique_name", using: :btree

  create_table "badge_reward_actions", force: :cascade do |t|
    t.integer "times_to_achieve"
    t.boolean "removed",                   default: false
    t.integer "badge_reward_condition_id"
    t.integer "action_id"
  end

  add_index "badge_reward_actions", ["action_id"], name: "index_badge_reward_actions_on_action_id", using: :btree
  add_index "badge_reward_actions", ["badge_reward_condition_id"], name: "index_badge_reward_actions_on_badge_reward_condition_id", using: :btree

  create_table "badge_reward_conditions", force: :cascade do |t|
    t.string  "condition_type"
    t.integer "badge_id"
  end

  add_index "badge_reward_conditions", ["badge_id"], name: "index_badge_reward_conditions_on_badge_id", using: :btree

  create_table "badges", force: :cascade do |t|
    t.string   "name"
    t.integer  "bonus_points"
    t.boolean  "active"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "removed",            default: false
    t.integer  "game_id"
    t.string   "picture"
  end

  add_index "badges", ["game_id"], name: "index_badges_on_game_id", using: :btree

  create_table "games", force: :cascade do |t|
    t.string   "name"
    t.string   "unique_name"
    t.string   "objective"
    t.integer  "number_of_players",  default: 0
    t.integer  "number_of_actions",  default: 0
    t.integer  "number_of_badges",   default: 0
    t.integer  "number_of_levels",   default: 0
    t.integer  "user_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "removed",            default: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "picture"
  end

  add_index "games", ["unique_name"], name: "index_games_on_unique_name", using: :btree
  add_index "games", ["user_id"], name: "index_games_on_user_id", using: :btree

  create_table "levels", force: :cascade do |t|
    t.string   "name"
    t.integer  "required_points"
    t.boolean  "active"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "hidden",             default: false
    t.boolean  "removed",            default: false
    t.integer  "game_id"
    t.string   "picture"
  end

  add_index "levels", ["game_id"], name: "index_levels_on_game_id", using: :btree

  create_table "object_properties", force: :cascade do |t|
    t.string   "name"
    t.string   "kind"
    t.boolean  "removed",          default: false
    t.integer  "action_object_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "object_properties", ["action_object_id"], name: "index_object_properties_on_action_object_id", using: :btree

  create_table "performed_action_properties", force: :cascade do |t|
    t.string   "value"
    t.integer  "performed_action_id"
    t.integer  "object_property_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "performed_action_properties", ["object_property_id"], name: "index_performed_action_properties_on_object_property_id", using: :btree
  add_index "performed_action_properties", ["performed_action_id"], name: "index_performed_action_properties_on_performed_action_id", using: :btree

  create_table "performed_actions", force: :cascade do |t|
    t.integer  "action_id"
    t.integer  "player_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "performed_actions", ["action_id"], name: "index_performed_actions_on_action_id", using: :btree
  add_index "performed_actions", ["player_id"], name: "index_performed_actions_on_player_id", using: :btree

  create_table "player_badges", force: :cascade do |t|
    t.integer  "player_id"
    t.integer  "badge_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "player_badges", ["badge_id"], name: "index_player_badges_on_badge_id", using: :btree
  add_index "player_badges", ["player_id"], name: "index_player_badges_on_player_id", using: :btree

  create_table "players", force: :cascade do |t|
    t.string   "name"
    t.string   "social_media_id"
    t.integer  "points_obtained",    default: 0
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "removed",            default: false
    t.integer  "game_id"
    t.integer  "level_id"
    t.string   "picture"
  end

  add_index "players", ["game_id"], name: "index_players_on_game_id", using: :btree
  add_index "players", ["level_id"], name: "index_players_on_level_id", using: :btree

  add_foreign_key "action_objects", "actions"
  add_foreign_key "actions", "games"
  add_foreign_key "badge_reward_actions", "actions"
  add_foreign_key "badge_reward_actions", "badge_reward_conditions"
  add_foreign_key "badge_reward_conditions", "badges"
  add_foreign_key "badges", "games"
  add_foreign_key "levels", "games"
  add_foreign_key "object_properties", "action_objects"
  add_foreign_key "performed_action_properties", "object_properties"
  add_foreign_key "performed_action_properties", "performed_actions"
  add_foreign_key "performed_actions", "actions"
  add_foreign_key "performed_actions", "players"
  add_foreign_key "player_badges", "badges"
  add_foreign_key "player_badges", "players"
  add_foreign_key "players", "games"
  add_foreign_key "players", "levels"
end

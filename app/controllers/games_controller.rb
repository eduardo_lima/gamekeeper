class GamesController < ApplicationController
  include GamesHelper

  # antes de qualquer ação, o usuário deverá estar logado
  before_action :require_user
  # verificar permissão de usuário

  before_action :load_game, only: [:show, :update, :destroy, :change_token]

  # before_action :check_permission!, only: []
  before_action :check_permission, only: [:show, :destroy]
  # antes de criar ou atualizar o game é preciso processar o parâmetro file
  before_action :process_params, only: [:create, :update]

  # Método para criar games (ou projetos, sistemas gamificados)
  # Retorna mensagem de game criado
  def create
    @game = Game.new(game_params)

    if @game.save
      render json: { message: I18n.t('.gamecontroller.actions.create.ok', name: @game.name) }, status: :ok 
    else
      respond_with @game
    end
  end

  # Método para atualizar games
  # Retorna JSON com mensagem do game atualizado
  def update
    @game = Game.find(params[:id])
    api_key = APIKey.find_by_unique_name(@game.unique_name)

    if @game.update_attributes(game_params)
      api_key.unique_name = @game.unique_name
      api_key.save
      render json: { message: I18n.t('.gamecontroller.actions.update.ok', name: @game.name) }, status: :ok 
    else
      respond_with @game
    end
  end

  def change_token
    api_key = APIKey.find_by_unique_name(@game.unique_name)
    api_key.access_token = SecureRandom.hex
    if api_key.save
      render json: { token: api_key.access_token }, status: :ok 
    else
      respond_with @game
    end
  end

  # Método para listar os games do usuário
  # Retorna hash com as informações básicas e estatísticas do game
  def index
    respond_with Game.list(@current_user.id).collect { |g| g.to_hash([:basic, :stats]) }
  end

  # Método para obter projeto específico com id
  # Retorna hash com informações básicas do game
  def show
    respond_with @game.to_hash(:basic)
  end

  # Método para deletar o game
  # Retorna mensagem se removeu game
  def destroy
    if @game.remove_this
      # api_key = APIKey.find_by_unique_name(@game.unique_name)
      # api_key.destroy
      respond_with do |format|
        format.json { render json: { message: I18n.t('.gamecontroller.actions.destroy.ok', name: @game.name), :id => @game.id },status: :ok }
      end
    else
      respond_with do |format|
        format.json { render json: { message: I18n.t('.gamecontroller.actions.destroy.internal_server_error')}, status: :internal_server_error }
      end
    end
  end

private

  def game_params
    params.require(:game).permit(:user_id, :name, :objective, :unique_name, :image, :picture)
  end

  def process_params

    puts "CHAMOUUUUUUUUUUUUUUUU"

    puts params.inspect
    puts params[:file].inspect

    params[:game] = JSON.parse(params[:game]).with_indifferent_access if hash.is_a?(String)

    if params[:file]
      params[:game][:image] = params[:file]
      params[:game][:picture] = params[:file]
    end
  end

  # Método para verificar se game pertence ao usuário logado
  # Retorna mensagem de validação
  def check_permission!
    user_id = @current_user.id
    unique_name = game_params[:id] 

    @game = Game.find_by(unique_name: unique_name)   
    unless user_id == @game.user_id
      render json: { message: I18n.t('.gamecontroller.filters.check_permission') }, status: :unauthorized
    end
  end

  # Método para verificar se game pertence ao usuário logado a partir de um id
  # Retorna mensagem de validação
  def check_permission
    user_id = @current_user.id
    unique_name = params[:id] 

    #  @game = Game.find_by(unique_name: unique_name) 
    unless user_id == @game.user_id
      render json: { message: I18n.t('.gamecontroller.filters.check_permission') }, status: :unauthorized
    end
  end

  def load_game
    @game = Game.find_by(unique_name: params[:id], removed: false)
  end
  
end

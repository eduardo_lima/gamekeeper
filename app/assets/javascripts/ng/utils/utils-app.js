(function () {
  
  /**
   * Módulo de recursos uteis em toda aplicacao
   *
   * @module utils
   * @requires users.controllers Módulo com controllers
   * @requires users.factories Módulo com factories
   */
  angular.module('gamekeeper.utils', [
    'gamekeeper.utils.controllers',
    'gamekeeper.utils.directives', 
    'gamekeeper.utils.factories',
    'gamekeeper.utils.misc'
  ]);
  
})();



class CreateBadges < ActiveRecord::Migration
  def change
    create_table :badges do |t|
      t.string  :name
      t.integer  :bonus_points
      t.boolean :active
      t.attachment :image

      t.boolean :removed, default: false

      t.references :game, index: true, foreign_key: true
    end
  end
end
